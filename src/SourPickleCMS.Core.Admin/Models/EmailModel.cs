﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SourPickleCMS.Core.Admin.Models
{
    public class EmailModel
    {
        public string to { get; set; }
        public string fromname { get; set; }
        public string fromaddress { get; set; }
        public string subject { get; set; }
        public string message { get; set; }
    }
}