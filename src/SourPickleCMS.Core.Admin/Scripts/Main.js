var new_block_region = null;
var dont_delete_block = false;

$(document).ready(function () {

    var curr_menu = $("#current_menu").val();
    $("#sidebar").find("a").each(function () {
        if ($(this).parent().attr("name") == curr_menu) {
            $(this).parent().addClass("current");
            var par = $(this).parent().parent().parent();
            if ($(par).attr("id") != "sidebar") {
                $(par).addClass("opened current");
                $(par).find('ul:first').slideDown(10);
            } else {
                $(this).parent().find('ul:first').slideDown(10);
            }
        }
    });

    $("#sortable").sortable({
        placeholder: 'menu_placeholder',
        forceHelperSize: true,
        update: on_menu_change
    });
    $("#sortable").disableSelection();

    $(".blocks_sortable").sortable({
        placeholder: 'block_placeholder',
        containment: 'parent',
        revert: true,
        update: function() {
            $(this).find(".delete_link").show();
            save_blocks_order(this);
        },
        receive: function(event, ui) {
            show_block_form(ui.item, this);
        }
	}).disableSelection();
    $(".blocks_draggable").draggable({
	    connectToSortable: ".blocks_sortable",
        helper: 'clone',
        revert: 'invalid',
        appendTo: 'body'
	}).disableSelection();
    $(".blocks_sortable .block_container").bind('click', function() {
        dont_delete_block = true;
        show_block_form(this, $(this).parent());
    });

    $(".menu_edit").bind("click", function() {
        on_menu_item_edit(this);
    });

    $(".custom_field_entry").each(function() {
        $(this).find(".post_field_update").bind('click', function() {
            var parent = $(this).parent().parent();
            update_custom_field(parent);
        });
        $(this).find(".post_field_delete").bind('click', function() {
            var parent = $(this).parent().parent();
            delete_custom_field(parent);
        });
    });

    $(".post_entry").bind("mouseover", function () {
        $(this).find(".post_entry_actions").css({ visibility: "visible" });
    });
    $(".post_entry").bind("mouseout", function () {
        $(this).find(".post_entry_actions").css({ visibility: "hidden" });
    });

    /**
    * Slide toggle for blocs
    * */
    $('.bloc .title .tabs').parent().find('.toggle').remove();
    $('.bloc .title .toggle').click(function () {
        $(this).toggleClass('hide').parent().parent().find('.content').slideToggle(300);
        return false;
    });

    /**
    * Jquery UI 
    * Automate jQuery UI insertion (no need to add more code)(and unfirm)
    * input.datepicker become a datepicker
    * input.range become a slider (value is inserted in the input) 
    **/
    $(".datepicker").datepicker();

    $('.range').each(function () {
        var cls = $(this).attr('class');
        var matches = cls.split(/([a-zA-Z]+)\-([0-9]+)/g);
        var options = {
            animate: true
        };
        var elem = $(this).parent();
        elem.append('<div class="uirange"></div>');
        for (i in matches) {
            i = i * 1;
            if (matches[i] == 'max') {
                options.max = matches[i + 1] * 1
            }
            if (matches[i] == 'min') {
                options.min = matches[i + 1] * 1
            }
        }
        options.slide = function (event, ui) {
            elem.find('span:first').empty().append(ui.value);
            elem.find('input:first').val(ui.value);
        }
        elem.find('span:first').empty().append(elem.find('input:first').val());
        options.range = 'min';
        options.value = elem.find('input:first').val();
        elem.find('.uirange').slider(options);
        $(this).hide();
    });


    /**
    * Autohide errors when an input with error is focused
    * */
    $('.input.error input,.input textarea,.input select').focus(function () {
        $(this).parent().removeClass('error');
        $(this).parent().find('.error-message').fadeTo(500, 0).slideUp();
        $(this).unbind('focus');
    });

    /**
    * CheckAll, if the checkbox with checkall class is checked/unchecked all checkbox would be checked
    * */
    $('#content .checkall').change(function () {
        $(this).parents('table:first').find('input').attr('checked', $(this).is(':checked'));
    });


    /** 
    * Sidebar menus
    * Slidetoggle for menu list
    * */
    var currentMenu = null;
    $('#sidebar>ul>li').each(function () {
        if ($(this).find('li').length == 0) {
            $(this).addClass('nosubmenu');
        }
    })
    $('#sidebar>ul>li:not([class*="current"])>ul').hide();
    $('#sidebar>ul>li:not([class*="nosubmenu"])>a').click(function () {
        e = $(this).parent();
        if ($(e).hasClass("opened")) {
            $(e).removeClass("opened current").addClass('closed').find('ul:first').slideUp();
        } else {
            $(e).removeClass("closed").addClass('opened current').find('ul:first').slideDown();
        }
    });

    var htmlCollapse = $('#menucollapse').html();
    if ($.cookie('isCollapsed') === 'true') {
        $('body').addClass('collapsed');
        $('#menucollapse').html('&#9654;');
    }
    $('#menucollapse').click(function () {
        var body = $('body');
        body.toggleClass('collapsed');
        isCollapsed = body.hasClass('collapsed');
        if (isCollapsed) {
            $(this).html('&#9654;');
        } else {
            $(this).html(htmlCollapse);
        }
        $.cookie('isCollapsed', isCollapsed);
        return false;
    });

    $('.close').click(function () { $(this).parent().fadeTo(500, 0).slideUp('slow', function () { $(this).remove(); }); });

    if ($("#msg_box").length > 0) {
        setTimeout(function () {
            $("#msg_box").slideUp('slow', function () { $(this).remove(); });
        }, 8000);
    }
});

function flash_message(type, message) {
    var msg = $("<div id='msg_box' class='notif " + type + " bloc' style='display: none'>" + message + "<a href='#' class='close'>x</a></div>");

    $("#content").prepend(msg);

    $(msg).slideDown('slow', function() {
        setTimeout(function () {
            $("#msg_box").slideUp('slow', function () { $(this).remove(); });
        }, 8000);
    });
}

function show_block_form(el, region) {
    $("#block_settings_modal").modal({
        overlayClose: true,
        opacity: 50,
        closeHTML: "CLOSE",
        onOpen: function (dialog) {
            dialog.container.fadeIn('fast');
            dialog.overlay.fadeIn('fast', function () {
                $.ajax({
                    url: "/Admin/Ajax/GetBlockFormAsString?id=" + ((dont_delete_block == false) ? $(el).find('.name').val() : $(el).find('.id').val()) + "&dir=" + $(el).find('.dir').val(),
                    dataType: "html",
                    error: function () { $.modal.close(); },
                    type: "GET",
                    success: function (data) {
                        $("#block_form_content").html(data);
                        $("#block_form_content").find("input").each(function() {
                            $(this).attr("name", "Fields." + $(this).attr("name"));
                        });
                        $("#block_form_content").find("textarea").each(function() {
                            $(this).attr("name", "Fields." + $(this).attr("name"));
                        });
                        $("#block_settings_modal").find(".hidden_fields").html($(el).find(".hidden_fields").html());
                        $(region).children().each(function() {
                            if ($(this).find(".id").val() == "") {
                                $("#block_dialog_content").find(".order").val($(this).find(".order").val());
                            }
                        });
                        $("#block_dialog_content").find(".region").val($(region).parent().attr("region"));
                        $("#block_settings_modal").find("h2").html($(el).find("h5").html());
                        if (!dont_delete_block) new_block_region = region;
                        dialog.data.fadeIn('fast');
                    }
                });
            });
        },
        onClose: function (dialog) {
            dialog.container.fadeOut('fast', function () {
                dialog.overlay.fadeOut('fast', function () {
                    $("#block_form_content").html("");
                    $("#block_dialog_content .hidden_fields").html("");
                    $.modal.close();
                });
            });
        }
    });
}

function cancel_new_block() {
    if (!dont_delete_block) {
        $(new_block_region).children().each(function() {
            if ($(this).find(".id").val() == "") {
                $(this).slideUp(function() {
                    $(this).remove();
                    new_block_region = null;
                    $.modal.close();
                });
            }
        });
    }
    dont_delete_block = false;
    $.modal.close();
}

function save_blocks_order(el) {
    var counter = 0;
    var json = [];
    $(el).children().each(function() {
        $(this).find(".order").val(counter);
        counter++;
        if ($(this).find(".id").val() == "") return;
        json.push({ Id: $(this).find(".id").val(), Order: $(this).find(".order").val() });
    });
    if (json.length == 0) return;

    $.ajax({
        url: "/Admin/Ajax/UpdateBlocksOrder?json=" + $.toJSON(json),
        dataType: "html",
        error: function () { alert("Error updating blocks order."); },
        type: "POST",
        success: function (data) {
            if (data != "SUCCESS") {
                alert("Error updating blocks order.");
            }
        }
    });
}

function on_menu_change() {
    var counter = 0;
    $("#sortable").children().each(function() {
        $(this).attr("order", counter);

        if ($(".field_container").length > 0) {
            $(this).find(".order").val(counter);
            var pos = $(this).find(".order").attr("name").indexOf(".");

            $(this).find(".order").attr("name", "Fields[" + counter + "].Order");
            $(this).find(".name").attr("name", "Fields[" + counter + "].Name");
            $(this).find(".type").attr("name", "Fields[" + counter + "].Type");
            $(this).find(".group_name").attr("name", "Fields[" + counter + "].GroupName");
        }
        counter++;
    });
}

function on_menu_item_edit(e) {
    var el = $(e).parent().parent();

    $("#edit_menu_bloc #PostId").val($(el).attr("postid"));
    $("#edit_menu_bloc #MenuPostID").val($(el).attr("menupostid"));
    $("#edit_menu_bloc #Link").val($(el).attr("link"));
    $("#edit_menu_bloc #Title").val($(el).find(".menu_name").html());
    $("#edit_menu_bloc").slideDown('slow');
}

function edit_menu_item() {
    var form = $("#edit_menu_bloc");
    var title = $(form).find("#Title").val();
    var menuPostID = $(form).find("#MenuPostID").val();
    var link = $(form).find("#Link").val();
    var postID = $(form).find("#PostId").val();
    if ($.trim(title) == "" || ($.trim(menuPostID) == "" && $.trim(link) == "")) {
        $("#edit_menu_item_msg").html("Please complete all fields.");
        return;
    }
    $("#edit_menu_item_msg").html("Updating menu item...");

    $.ajax({
        url: "/Admin/Ajax/UpdateMenuItem?id=" + postID + "&title=" + title + "&menupostid=" + menuPostID + "&link=" + link.replace('#', '%23'),
        dataType: "json",
        error: function () { $.modal.close(); },
        type: "POST",
        success: function (data) {
            if (data == "ERROR") {
                $("#edit_menu_item_msg").html("Error adding new item.");
            } else {
                $(form).find("#Title").val("");
                $(form).find("#MenuPostID").val("");
                $(form).find("#Link").val("");
                $(form).find("#PostID").val("");

                $(".menu_item").each(function() {
                    if ($(this).attr("postid") == data.PostID) {
                        $(this).find(".menu_name").html(data.Title);
                        $(this).attr("menupostid", data.MenuPostID);
                        $(this).attr("link", data.Link);
                    }
                });
                $("#edit_menu_item_msg").html("Menu item updated successfully.");
            }
            setTimeout(function() { $("#edit_menu_item_msg").html(""); $("#edit_menu_bloc").slideUp('slow'); }, 4000);
        }
    });
}

function new_field_item() {
    var form = $("#new_field_form");
    var title = $(form).find("#FieldName").val();
    var gname = $(form).find("#GroupName").val();
    var type = $(form).find("#FieldType").val();

    var el = $("#blank_field_item").clone();
    $(el).removeAttr("id").addClass("field_item");
    $(el).find(".group_name").val(gname);
    $(el).find(".name").val(title);
    $(el).find(".type").val(type);

    $(".field_container").append(el);
    $(el).slideDown('slow');
    $(el).find(".delete_link").bind("click", function() { delete_field_item(this); });

    on_menu_change();
}

function delete_field_item(el) {
    var field = $(el).parent();
    $(field).slideUp('slow', function() {
        $(this).remove();
        on_menu_change();
    });
}

function new_menu_item() {
    var form = $("#new_menu_form");
    var title = $(form).find("#Title").val();
    var menuPostID = $(form).find("#MenuPostID").val();
    var link = $(form).find("#Link").val();
    var categorySlug = $("#Category_Slug").val();
    if ($.trim(title) == "" || ($.trim(menuPostID) == "" && $.trim(link) == "")) {
        $("#add_menu_item_msg").html("Please complete all fields.");
        return;
    }
    $("#add_menu_item_msg").html("Adding new item...");

    $.ajax({
        url: "/Admin/Ajax/AddNewMenuItem?title=" + title + "&id=" + menuPostID + "&catslug=" + categorySlug + "&link=" + link.replace('#', '%23') + "&order=" + $(".menu_container li:last-child").attr("order"),
        dataType: "json",
        error: function () { $.modal.close(); },
        type: "POST",
        success: function (data) {
            if (data == "ERROR") {
                $("#add_menu_item_msg").html("Error adding new item.");
            } else {
                $(form).find("#Title").val("");
                $(form).find("#MenuPostID").val("");
                $(form).find("#Link").val("");

                var el = $("<li class='menu_item' postid='" + data.PostID + "' link='" + data.Link + "' order='' menupostid='" + data.MenuPostID + "' style='display: none'><div class='menu_name'>" + data.Title + "</div><div class='menu_links'><a class='menu_edit' href='javascript://'>edit</a> | <a class='menu_item_delete' href='/Admin/Main/DeleteMenuItem/" + data.PostID + "?catslug=" + data.CategorySlug + "'>delete</a></div></li>");
                $(".menu_container").append(el);
                $(el).slideDown('slow');
                $(el).find(".menu_edit").bind("click", function() { on_menu_item_edit(this); });

                on_menu_change();
                $("#add_menu_item_msg").html("Menu item added successfully.");
            }
            setTimeout(function() { $("#add_menu_item_msg").html(""); }, 4000);
        }
    });
}

function save_menu_changes() {
    var json = [];

    $(".menu_item").each(function() {
        var data = {
            Id: $(this).attr("postid"),
            Order: $(this).attr("order")
        };
        json.push(data);
    });
    $("#menu_update_msg").html("Saving...");

    $.ajax({
        url: "/Admin/Ajax/UpdateMenuOrder?json=" + $.toJSON(json),
        dataType: "html",
        error: function () { $.modal.close(); },
        type: "POST",
        success: function (data) {
            if (data == "SUCCESS") {
                $("#menu_update_msg").html("Menu order updated successfully!");
            } else {
                $("#menu_update_msg").html(data);
            }
            setTimeout(function() { $("#menu_update_msg").html(""); }, 8000);
        }
    });
}

function edit_menu() {
    window.location.href = "/Admin/Main/Menus/" + $("#menu_list").val();
}

function confirm_delete_menu(id, ptid) {
    $("#modal_dialog").modal({
        overlayClose: true,
        opacity: 50,
        closeHTML: "CLOSE",
        onOpen: function (dialog) {
            dialog.container.fadeIn('fast');
            dialog.overlay.fadeIn('fast', function () {
                $.ajax({
                    url: "/Admin/Ajax/GetConfirmDeleteMenuDialog?slug=" + $("#Category_Slug").val(),
                    dataType: "html",
                    error: function () { $.modal.close(); },
                    type: "GET",
                    success: function (data) {
                        $("#dialog_content").html(data);
                        dialog.data.fadeIn('fast');
                    }
                });
            });
        },
        onClose: function (dialog) {
            dialog.container.fadeOut('fast', function () {
                dialog.overlay.fadeOut('fast', function () {
                    $("#dialog_content").html("");
                    $.modal.close();
                });
            });
        }
    });
}

function confirm_delete_single(id, ptid) {
    $("#modal_dialog").modal({
        overlayClose: true,
        opacity: 50,
        closeHTML: "CLOSE",
        onOpen: function (dialog) {
            dialog.container.fadeIn('fast');
            dialog.overlay.fadeIn('fast', function () {
                $.ajax({
                    url: "/Admin/Ajax/GetConfirmDeleteSingleDialog?id=" + id + "&ContentType=" + ptid,
                    dataType: "html",
                    error: function () { $.modal.close(); },
                    type: "GET",
                    success: function (data) {
                        $("#dialog_content").html(data);
                        dialog.data.fadeIn('fast');
                    }
                });
            });
        },
        onClose: function (dialog) {
            dialog.container.fadeOut('fast', function () {
                dialog.overlay.fadeOut('fast', function () {
                    $("#dialog_content").html("");
                    $.modal.close();
                });
            });
        }
    });
}

function confirm_delete_user(id) {
    $("#modal_dialog").modal({
        overlayClose: true,
        opacity: 50,
        closeHTML: "CLOSE",
        onOpen: function (dialog) {
            dialog.container.fadeIn('fast');
            dialog.overlay.fadeIn('fast', function () {
                $.ajax({
                    url: "/Admin/Ajax/GetConfirmDeleteUserDialog?id=" + id,
                    dataType: "html",
                    error: function () { $.modal.close(); },
                    type: "GET",
                    success: function (data) {
                        $("#dialog_content").html(data);
                        dialog.data.fadeIn('fast');
                    }
                });
            });
        },
        onClose: function (dialog) {
            dialog.container.fadeOut('fast', function () {
                dialog.overlay.fadeOut('fast', function () {
                    $("#dialog_content").html("");
                    $.modal.close();
                });
            });
        }
    });
}

function new_category_dialog() {
    $("#modal_dialog").modal({
        overlayClose: true,
        opacity: 50,
        closeHTML: "CLOSE",
        onOpen: function (dialog) {
            dialog.container.fadeIn('fast');
            dialog.overlay.fadeIn('fast', function () {
                $.ajax({
                    url: "/Admin/Ajax/GetNewAjaxCategoryView?id=" + $("#Post_ContentTypeID").val(),
                    dataType: "html",
                    error: function () { $.modal.close(); },
                    type: "GET",
                    success: function (data) {
                        $("#dialog_content").html(data);
                        dialog.data.fadeIn('fast');
                    }
                });
            });
        },
        onClose: function (dialog) {
            dialog.container.fadeOut('fast', function () {
                dialog.overlay.fadeOut('fast', function () {
                    $("#dialog_content").html("");
                    $.modal.close();
                });
            });
        }
    });
}

function add_new_category() {
    var params = { Name: $("#Name").val(), Slug: $("#Slug").val(), DefaultTemplate: $("#DefaultTemplate").val(), ContentTypeID: $("#ContentTypeID").val() };
    $.ajax({
        url: "/Admin/Ajax/AddCategory",
        data: params,
        dataType: "json",
        type: "POST",
        success: function (data) {
            var option = $("<option value='" + data.CategoryID + "'>" + data.Slug + "</option>");
            $("#Post_CategoryID").append(option);
            $.modal.close();
            flash_message("tip", "New category was added successfully.");
        },
        error: function () { },
    });
}

function confirm_delete_category(id, ptid) {
    $("#modal_dialog").modal({
        overlayClose: true,
        opacity: 50,
        closeHTML: "CLOSE",
        onOpen: function (dialog) {
            dialog.container.fadeIn('fast');
            dialog.overlay.fadeIn('fast', function () {
                $.ajax({
                    url: "/Admin/Ajax/GetConfirmDeleteCatalogDialog?id=" + id + "&ContentType=" + ptid,
                    dataType: "html",
                    error: function () { $.modal.close(); },
                    type: "GET",
                    success: function (data) {
                        $("#dialog_content").html(data);
                        dialog.data.fadeIn('fast');
                    }
                });
            });
        },
        onClose: function (dialog) {
            dialog.container.fadeOut('fast', function () {
                dialog.overlay.fadeOut('fast', function () {
                    $("#dialog_content").html("");
                    $.modal.close();
                });
            });
        }
    });
}

function add_custom_field() {
    if ($("#Post_Id").val() == "") {
        $("#custom_field_status_message").html("Save this post before adding custom fields.");
        return false;
    }
    if ($.trim($("#custom_field_name_select").val()) == "" && $.trim($("#custom_field_name").val()) == "") {
        $("#custom_field_status_message").html("Enter or select a name.");
        return false;
    }
    if ($.trim($("#custom_field_value").val()) == "") {
        $("#custom_field_status_message").html("Enter a value.");
        return false;
    }
    $("#custom_field_status_message").html("");
    $("#custom_field_loader").css("display", "inline-block");
    var name = ""
    if ($.trim($("#custom_field_name").val()) != "") {
        name = $("#custom_field_name").val();
    } else {
        name = $("#custom_field_name_select").val();
    }
    var params = { name: name, value: $("#custom_field_value").val(), postid: $("#Post_Id").val() };
    
    $.ajax({
        url: "/Admin/Ajax/NewCustomField",
        dataType: "json",
        error: function () { $("#custom_field_status_message").html("There was an error.  Please try again."); },
        data: params,
        type: "POST",
        success: function (data) {
            if (data == "") {
                $("#custom_field_status_message").html("There was an error.  Please try again.");
            } else {
                var el = $(".custom_field_blank").clone().removeClass("custom_field_blank");
                $(el).find(".post_field_name").val(data.Name);
                $(el).find(".post_field_value").val(data.Value);
                $(el).find(".post_field_update").bind('click', function() {
                    update_custom_field($(this).parent().parent());
                });
                $(el).find(".post_field_delete").bind('click', function() {
                    delete_custom_field($(this).parent().parent());
                });
                $("#custom_field_container").append(el);
                $("#custom_field_container").append("<br />");
                $(el).slideDown('slow').fadeIn('slow');
            }
            $("#custom_field_loader").css("display", "none");
        }
    });
}

function update_custom_field(el) {
    var name = $(el).find(".post_field_name").val();
    var value = $(el).find(".post_field_value").val();

    if ($.trim(name) == "") return false;
    $(el).find(".custom_field_status").attr("src", "/Areas/Admin/Content/Css/img/loader.gif").css("display", "inline-block");
    var params = { name: name, value: value, postid: $("#Post_Id").val() };
    
    $.ajax({
        url: "/Admin/Ajax/UpdateCustomField",
        dataType: "text",
        error: function (jqXHR, textStatus, errorThrown) { 
            $(el).find(".custom_field_status").attr("src", "/Areas/Admin/Content/Css/img/error.png").css("display", "inline-block"); 
        },
        data: params,
        type: "POST",
        success: function (data) {
            if (data != "SUCCESS") {
                $(el).find(".custom_field_status").attr("src", "/Areas/Admin/Content/Css/img/error.png").css("display", "inline-block");
            }
            $(".custom_field_status").attr("src", "/Areas/Admin/Content/Css/img/success.png");
            setTimeout(function() { $(el).find(".custom_field_status").fadeOut(); }, 5000);
        }
    });
}

function delete_custom_field(el) {
    var name = $(el).find(".post_field_name").val();
    var value = $(el).find(".post_field_value").val();
    $(el).find(".custom_field_status").attr("src", "/Areas/Admin/Content/Css/img/loader.gif").css("display", "inline-block");
    var params = { name: name, value: value, postid: $("#Post_Id").val() };
    
    $.ajax({
        url: "/Admin/Ajax/DeleteCustomField",
        dataType: "text",
        error: function () { $(el).find(".custom_field_status").attr("src", "/Areas/Admin/Content/Css/img/error.png").css("display", "inline-block"); },
        data: params,
        type: "POST",
        success: function (data) {
            if (data != "SUCCESS") {
                $(el).find(".custom_field_status").attr("src", "/Areas/Admin/Content/Css/img/error.png").css("display", "inline-block");
            }
            $(el).slideUp(function() { $(this).remove(); });
        }
    });
}

function send_test_email() {
    if ($.trim($(".test_to_email").val()) == "") {
        $(".test_email_status").html("Enter an address to send test too.");
        setTimeout(function () { $(".test_email_status").html(""); }, 6000);
        return;
    }
    var fields = {
        to: $(".test_to_email").val(),
        subject: "Test email sent too " + $(".test_to_email").val(),
        message: "This is a test email sent from Sour Pickle CMS",
        fromname: "",
        fromaddress: ""
    };

    $(".test_email_status").html("Sending...");
    $.ajax({
        url: "/Admin/Ajax/SendEmail",
        type: "POST",
        data: $.toJSON(fields),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        success: function(data) {
            $(".test_email_status").html("Message was sent successfully.");
            setTimeout(function () { $(".test_email_status").html(""); }, 6000);
        },
        error: function() {
            $(".test_email_status").html("There was an error sending the message.");
            setTimeout(function () { $(".test_email_status").html(""); }, 6000);
        }
    });
}