﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using SourPickleCMS.Core.Attributes;
using SourPickleCMS.Core.Controllers;
using SourPickleCMS.Core.Helpers;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Models.ViewModels;

namespace SourPickleCMS.Core.Admin.Controllers
{
    [RoleAuthorize(Roles = AuthRoles.Writer)]
    public class MainController : SourPickleController
    {
        public ActionResult Index()
        {
            AdminDashboardViewModel model = new AdminDashboardViewModel();

            model.ThemeName = unitOfWork.Session.Settings["Theme"];
            model.SiteVersion = ConfigurationManager.AppSettings["SourPickleVersion"];

            model.PageCount = unitOfWork.PostRepository.GetPostCountByType("page");
            model.PostCount = unitOfWork.PostRepository.GetPostCountByType("blog");
            model.UserCount = unitOfWork.AccountRepository.AccountCount();
            model.CommentCount = unitOfWork.PostRepository.TotalCommentsCount();
            //model.PendingCommentCount = unitOfWork.CommentRepository.Get(c => c.IsApproved == false && c.IsDeleted == false).Count();
            //model.RecentComments = unitOfWork.CommentRepository.Get(orderBy: rc => rc.OrderByDescending(o => o.CreateDate)).Take(5);

            return View(model);
        }

        public ActionResult Editor()
        {
            return View();
        }

        #region Blocks

        public ActionResult Blocks()
        {
            BlocksViewModel model = new BlocksViewModel();
            model.ThemeBlocks = unitOfWork.BlockRepository.GetBlocksByTheme(unitOfWork.Session.Settings["Theme"]);

            model.AdminBlocks = ThemeHelpers.GetBlocks(Server.MapPath("~/Areas/Admin/Blocks"));
            model.UserBlocks = ThemeHelpers.GetBlocks(Server.MapPath("~/Blocks"));
            model.Regions = ThemeHelpers.GetRegions(Server.MapPath("~/Themes/" + unitOfWork.Session.Settings["Theme"]));

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [BypassAntiForgeryTokenAttribute]
        public ActionResult SaveBlock(Block model)
        {
            model.Theme = unitOfWork.Session.Settings["Theme"];
            if (model.Id == null)
            {
                unitOfWork.BlockRepository.Save(model);
                TempData["msg"] = "Block added successfully.";
            }
            else
            {
                unitOfWork.BlockRepository.Update(model);
                TempData["msg"] = "Block updated successfully.";
            }
            return RedirectToAction("Blocks");
        }

        #endregion

        #region ContentTypes

        public ActionResult ContentTypes()
        {
            IList<ContentType> model = unitOfWork.ContentTypeRepository.GetCustomContentTypes();

            return View(model);
        }

        public ActionResult NewContentType()
        {
            return View("EditContentType", new ContentType());
        }

        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult EditContentType([DefaultValue("")]string slug)
        {
            ContentType model = unitOfWork.ContentTypeRepository.GetBySlug(slug);
            return View(model);
        }

        [HttpPost]
        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult EditContentType(ContentType model)
        {
            if (String.IsNullOrWhiteSpace(model.Name))
            {
                TempData["error"] = "No Content Type name provided.  Please try again.";
                return View(model);
            }
            if (String.IsNullOrWhiteSpace(model.PluralName))
            {
                TempData["error"] = "No Content Type plural name provided.  Please try again.";
                return View(model);
            }
            if (String.IsNullOrWhiteSpace(model.Slug))
            {
                TempData["error"] = "No Content Type slug provided.  Please try again.";
                return View(model);
            }
            model.IsCustom = true;
            IList<Field> fields = new List<Field>();

            if (model.Fields != null)
            {
                foreach (Field f in model.Fields.OrderBy(o => o.Order))
                {
                    if (String.IsNullOrWhiteSpace(f.Name)) continue;
                    f.IsCustomField = true;
                    fields.Add(f);
                }
            }
            else
            {
                model.Fields = new List<Field>();
            }

            if (model.Id == null)
            {
                unitOfWork.ContentTypeRepository.Save(model);
                TempData["msg"] = "Content Type saved successfully!";
            }
            else
            {
                unitOfWork.ContentTypeRepository.Update(model);
                TempData["msg"] = "Content Type updated successfully!";
            }
            return RedirectToAction("EditContentType", new { slug = model.Slug });
        }

        #endregion

        #region Themes

        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult Themes()
        {
            string currentTheme = unitOfWork.Session.Settings["Theme"];
            IList<ThemeViewModel> themes = new List<ThemeViewModel>();
            var themesFolder = new DirectoryInfo(Server.MapPath("~/Themes"));
            string[] names = themesFolder.GetDirectories("*", SearchOption.TopDirectoryOnly).Select(x => x.Name).OrderBy(x => x).ToArray();

            foreach (string name in names)
            {
                ThemeViewModel theme = new ThemeViewModel();
                StreamReader sr = null;
                try
                {
                    sr = new StreamReader(themesFolder + "/" + name + "/ThemeInfo.txt");
                }
                catch
                {
                    continue;
                }
                theme.Folder = name;
                theme.Info = new Dictionary<string, string>();
                if (currentTheme == name) theme.IsCurrent = true;

                do
                {
                    string line = sr.ReadLine();
                    if (String.IsNullOrWhiteSpace(line) == true || line.Substring(0, 1) == "*") continue;
                    string key = line.Substring(0, line.IndexOf(':'));
                    string value = line.Substring(line.IndexOf(':') + 1);

                    theme.Info.Add(key, value);
                } while (!sr.EndOfStream);

                themes.Add(theme);
                sr.Close();
            }
            return View(themes);
        }

        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult SetTheme(string name)
        {
            if (!String.IsNullOrWhiteSpace(name)) {
                var themesFolder = new DirectoryInfo(Server.MapPath("~/Themes/" + name));
                try
                {
                    string[] names = themesFolder.GetFiles("*", SearchOption.TopDirectoryOnly).Select(x => x.Name).OrderBy(x => x).ToArray();

                    unitOfWork.SettingRepository.UpdateSetting("Theme", name);
                    unitOfWork.SettingRepository.AddThemeSettings(name, themesFolder.FullName);
                    unitOfWork.Session.Settings = unitOfWork.SettingRepository.GetSettingsDictionary();

                    TempData["msg"] = "New theme set successfully.";
                }
                catch (Exception err)
                {
                    if (err is DirectoryNotFoundException)
                    {
                        TempData["error"] = "The specified theme does not exist.";
                    }
                    else
                    {
                        TempData["error"] = "There was an error setting the new theme.";
                    }
                }
            }

            return RedirectToAction("Themes");
        }

        #endregion

        #region Settings

        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult GeneralSettings()
        {
            return View(unitOfWork.Session.Settings);
        }

        [HttpPost]
        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult GeneralSettings(FormCollection values)
        {
            foreach (string key in values.Keys)
            {
                if (key.Substring(0, 1) == "_") continue;
                string[] rval = (string[])values.GetValue(key).RawValue;
                unitOfWork.SettingRepository.UpdateSetting(key, rval[0], false);
                unitOfWork.Session.Settings[key] = rval[0];
            }
            unitOfWork.Save();
            TempData["msg"] = "Settings updated successfully.";

            return RedirectToAction("GeneralSettings");
        }

        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult UserSettings()
        {
            return View(unitOfWork.Session.Settings);
        }

        [HttpPost]
        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult UserSettings(FormCollection values)
        {
            foreach (string key in values.Keys)
            {
                if (key.Substring(0, 1) == "_") continue;
                string[] rval = (string[])values.GetValue(key).RawValue;
                unitOfWork.SettingRepository.UpdateSetting(key, rval[0], false);
                unitOfWork.Session.Settings[key] = rval[0];
            }
            unitOfWork.Save();
            TempData["msg"] = "Settings updated successfully.";

            return RedirectToAction("UserSettings");
        }

        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult DisplaySettings()
        {
            return View(unitOfWork.Session.Settings);
        }

        [HttpPost]
        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult DisplaySettings(FormCollection values)
        {
            foreach (string key in values.Keys)
            {
                if (key.Substring(0, 1) == "_") continue;
                string[] rval = (string[])values.GetValue(key).RawValue;
                unitOfWork.SettingRepository.UpdateSetting(key, rval[0], false);
                unitOfWork.Session.Settings[key] = rval[0];
            }
            unitOfWork.Save();
            TempData["msg"] = "Settings updated successfully.";

            return RedirectToAction("DisplaySettings");
        }

        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult EmailSettings()
        {
            return View(unitOfWork.Session.Settings);
        }

        [HttpPost]
        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult EmailSettings(FormCollection values)
        {
            foreach (string key in values.Keys)
            {
                if (key.Substring(0, 1) == "_") continue;
                string[] rval = (string[])values.GetValue(key).RawValue;
                unitOfWork.SettingRepository.UpdateSetting(key, rval[0], false);
                unitOfWork.Session.Settings[key] = rval[0];
            }
            unitOfWork.Save();
            TempData["msg"] = "Settings updated successfully.";

            return RedirectToAction("EmailSettings");
        }

        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult ThemeSettings()
        {
            IList<Setting> model = unitOfWork.SettingRepository.GetThemeSettings(unitOfWork.Session.Settings["Theme"]);

            if (model.Count() == 0)
            {
                if (unitOfWork.SettingRepository.AddThemeSettings(unitOfWork.Session.Settings["Theme"], new DirectoryInfo(Server.MapPath("~/Themes/" + unitOfWork.Session.Settings["Theme"])).FullName))
                {
                    unitOfWork.Session.Settings = unitOfWork.SettingRepository.GetSettingsDictionary();
                    model = unitOfWork.SettingRepository.GetThemeSettings(unitOfWork.Session.Settings["Theme"]);
                }
            }

            return View(model);
        }

        [HttpPost]
        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult ThemeSettings(FormCollection values)
        {
            foreach (string key in values.Keys)
            {
                if (key.Substring(0, 1) == "_") continue;
                string[] rval = (string[])values.GetValue(key).RawValue;
                unitOfWork.SettingRepository.UpdateThemeSetting(key, rval[0], unitOfWork.Session.Settings["Theme"], false);
                unitOfWork.Session.Settings[key] = rval[0];
            }
            unitOfWork.Save();
            TempData["msg"] = "Settings updated successfully.";

            return RedirectToAction("ThemeSettings");
        }

        #endregion

        #region Menus

        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult Menus([DefaultValue("")]string id)
        {
            MenuListViewModel model = new MenuListViewModel();
            model.Menus = unitOfWork.CategoryRepository.GetByContentType("menu-item").OrderBy(c => c.Id).ToList();

            if (model.Menus.Count() > 0)
            {
                if (id != "")
                {
                    IEnumerable<Category> currMenu = model.Menus.Where(m => m.Slug == id);
                    if (currMenu.Count() > 0)
                    {
                        model.CurrentMenu = currMenu.First();
                    }
                    else
                    {
                        model.CurrentMenu = model.Menus.First();
                    }
                }
                else
                {
                    model.CurrentMenu = model.Menus.First();
                }

                model.MenuItems = unitOfWork.PostRepository.GetPostsByCategorySlug(model.CurrentMenu.Slug);
            }
            return View(model);
        }

        [HttpPost]
        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult NewMenu(Category model)
        {
            if (String.IsNullOrWhiteSpace(model.Name) == true || String.IsNullOrWhiteSpace(model.Slug) == true)
            {
                TempData["error"] = "Please complete all required fields.";
                return RedirectToAction("Menus");
            }
            model.ContentType = "menu-item";
            unitOfWork.CategoryRepository.Save(model);
            unitOfWork.Save();
            TempData["msg"] = "New menu created successfully.";

            return RedirectToAction("Menus", new { id = model.Id });
        }

        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult DeleteMenu([DefaultValue("")]string slug)
        {
            if (slug != "")
            {
                Category menu = unitOfWork.CategoryRepository.GetBySlug(slug);
                if (menu != null)
                {
                    unitOfWork.CategoryRepository.Delete(menu);
                    IList<Post> menuItems = unitOfWork.PostRepository.GetPostsByTypeAndCategory(slug, "menu-item");
                    foreach (Post p in menuItems)
                    {
                        unitOfWork.PostRepository.Delete(p);
                    }
                    unitOfWork.Save();
                    TempData["msg"] = "Menu deleted successfully.";
                }
                else
                {
                    TempData["error"] = "Could not find the specified menu.";
                }
            }

            return RedirectToAction("Menus");
        }

        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult DeleteMenuItem([DefaultValue("")]string id, [DefaultValue("")]string catid)
        {
            if (id != "")
            {
                Post menuItem = unitOfWork.PostRepository.GetByID(id);
                if (menuItem != null)
                {
                    unitOfWork.PostRepository.Delete(menuItem);
                    unitOfWork.Save();
                    TempData["msg"] = "Menu item deleted successfully.";
                }
                else
                {
                    TempData["error"] = "Could not find the specified menu item.";
                }
            }

            return RedirectToAction("Menus", new { id = catid });
        }

        #endregion
    }
}