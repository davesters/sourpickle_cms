﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SourPickleCMS.Core.Services.DataAccess;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Attributes;
using System.ComponentModel;
using SourPickleCMS.Core.Controllers;

namespace SourPickleCMS.Core.Admin.Controllers
{
    [RoleAuthorize(Roles = AuthRoles.Admin)]
    public class UsersController : SourPickleController
    {
        public ActionResult Index()
        {
            IEnumerable<Account> model = unitOfWork.AccountRepository.Get();

            return View(model);
        }

        public ActionResult Edit([DefaultValue("")]string id)
        {
            Account model = new Account();

            try
            {
                model = unitOfWork.AccountRepository.GetByID(id);
            }
            catch
            {
                model = null;
            }
            if (model == null) return RedirectToAction("Index");
            model.Password = "";

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Account model)
        {
            Account account = unitOfWork.AccountRepository.GetByID(model.Id);

            account.Email = model.Email;
            account.Name = model.Name;
            account.Username = model.Username;
            account.LockedReason = model.LockedReason;
            account.IsLocked = model.IsLocked;
            account.IsSuspended = model.IsSuspended;
            account.Role = model.Role;
            account.EmailVerified = model.EmailVerified;

            if (!String.IsNullOrEmpty(model.Password))
            {
                model.Password = SecurityHelper.GetHashedString(model.Password + "$" + account.PasswordSalt);
            }

            unitOfWork.AccountRepository.Update(account);

            return View("Edit", model);
        }

        public ActionResult New()
        {
            Account model = new Account();
            return View("Edit", model);
        }

        [HttpPost]
        public ActionResult New(Account model)
        {
            if (String.IsNullOrEmpty(model.Email))
            {
                TempData["error"] = "Please enter an email address, and try again.";
                return View("Edit", model);
            }
            if (String.IsNullOrEmpty(model.Username))
            {
                TempData["error"] = "Please enter a username, and try again.";
                return View("Edit", model);
            }
            if (String.IsNullOrEmpty(model.Password))
            {
                TempData["error"] = "Please enter a password, and try again.";
                return View("Edit", model);
            }

            var test = unitOfWork.AccountRepository.Get(a => a.Email == model.Email || a.Username == model.Username);
            if (test.Count() > 0)
            {
                TempData["error"] = "A user already exists with this email or username.  Please try again.";
                return View("Edit", model);
            }

            Account account = unitOfWork.AccountRepository.CreateNewUser(model);

            if (account == null)
            {
                TempData["error"] = "There was an error saving the account.  Please try again.";
                return View("Edit", model);
            }

            TempData["msg"] = "New user was added successfully!";
            return RedirectToAction("Edit", new { id = account.Id });
        }

        [RoleAuthorize(Roles = AuthRoles.Admin)]
        public ActionResult Delete([DefaultValue("")]string id)
        {
            if (unitOfWork.AccountRepository.AccountCount() == 1)
            {
                TempData["error"] = "You cannot delete the last user.";
                return RedirectToAction("Index");
            }
            Account model = unitOfWork.AccountRepository.GetByID(id);
            if (model.Id == unitOfWork.Session.CurrentUser.Id)
            {
                TempData["error"] = "You cannot delete the same account you are logged in under.";
                return RedirectToAction("Index");
            }

            if (model != null)
            {
                unitOfWork.AccountRepository.Delete(model);
                unitOfWork.Save();
                TempData["msg"] = "User deleted successfully!";
            }
            else
            {
                TempData["error"] = "Could not find the specified user.";
            }
            return RedirectToAction("Index");
        }
    }
}