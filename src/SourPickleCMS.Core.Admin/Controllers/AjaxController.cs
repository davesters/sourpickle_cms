﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using SourPickleCMS.Core.Admin.Models;
using SourPickleCMS.Core.Attributes;
using SourPickleCMS.Core.Controllers;
using SourPickleCMS.Core.Helpers;
using SourPickleCMS.Core.Models;

namespace SourPickleCMS.Core.Admin.Controllers
{
    public class AjaxController : SourPickleController
    {
        #region General
        public string GetUniqueSlug(string title, string catslug, string ContentType)
        {
            string slug = GeneralHelper.GetUniqueSlug(title, catslug, ContentType);
            return slug;
        }

        public string CheckSlug(string slug, string catslug, string ContentType)
        {
            string newslug = GeneralHelper.CheckSlug(slug, catslug, ContentType);

            return newslug;
        }

        public string GetFileContents(string fname)
        {
            string fcontents = "";
            StreamReader sr = new StreamReader(Server.MapPath("~" + fname));
            fcontents = sr.ReadToEnd();
            sr.Close();
            return fcontents;
        }

        [HttpPost]
        [ValidateInput(false)]
        [BypassAntiForgeryTokenAttribute]
        public ActionResult SendEmail(EmailModel model)
        {
            SendEmail(model.to, model.fromname, model.fromaddress, model.subject, model.message, false);
            return Json(new { @status = "SUCCESS" });
        }
        #endregion 

        [HttpPost]
        [BypassAntiForgeryTokenAttribute]
        public string AddCategory(Category model)
        {
            unitOfWork.CategoryRepository.Save(model);
            unitOfWork.Save();

            JavaScriptSerializer oSerializer = new JavaScriptSerializer();

            string sJSON = oSerializer.Serialize(model);

            return sJSON;
        }

        [HttpPost]
        [BypassAntiForgeryTokenAttribute]
        public string NewCustomField(string name, string value, string postid)
        {
            JavaScriptSerializer oSerializer = new JavaScriptSerializer();
            Field field = new Field
            {
                IsCustomField = true,
                Name = name,
                Value = value
            };

            bool status = unitOfWork.PostRepository.AddCustomField(field, postid);
            if (status)
            {
                unitOfWork.Save();

                string sJSON = oSerializer.Serialize(field);
                return sJSON;
            }
            else
            {
                return "";
            }
        }

        [HttpPost]
        [BypassAntiForgeryTokenAttribute]
        public string UpdateCustomField(string name, string value, string postid)
        {
            Field field = new Field
            {
                IsCustomField = true,
                Name = name,
                Value = value,
                Type = null
            };

            bool status = unitOfWork.PostRepository.UpdateCustomField(field, postid);
            if (status)
            {
                unitOfWork.Save();
                return "SUCCESS";
            }
            else
            {
                return "";
            }
        }

        [HttpPost]
        [BypassAntiForgeryTokenAttribute]
        public string DeleteCustomField(string name, string value, string postid)
        {
            Field field = new Field
            {
                IsCustomField = true,
                Name = name,
                Value = value,
                Type = null
            };

            bool status = unitOfWork.PostRepository.DeleteCustomField(field, postid);
            if (status)
            {
                unitOfWork.Save();
                return "SUCCESS";
            }
            else
            {
                return "";
            }
        }

        [BypassAntiForgeryTokenAttribute]
        public string UpdateMenuOrder(string json)
        {
            if (String.IsNullOrWhiteSpace(json)) return "No data received. Please try again.";

            JavaScriptSerializer JSS = new JavaScriptSerializer();

            IList<Post> posts = JSS.Deserialize<IList<Post>>(json);
            if (posts.Count() > 0)
            {
                foreach (Post p in posts)
                {
                    Post post = unitOfWork.PostRepository.GetByID(p.Id);
                    post.Order = p.Order;
                    unitOfWork.PostRepository.Update(post);
                }
                unitOfWork.Save();
            }
            else
            {
                return "No menu objects found.  Please try again.";
            }
            return "SUCCESS";
        }

        [HttpPost]
        [BypassAntiForgeryTokenAttribute]
        public string AddNewMenuItem(string title, string id, string catslug, string link, int order)
        {
            Category category = unitOfWork.CategoryRepository.GetBySlug(catslug);

            Post menuItem = new Post
            {
                Id = null,
                ContentType = new ContentType
                {
                    Commentable = false,
                    DefaultTemplate = "",
                    Name = "Menu Item",
                    PluralName = "Menu Items",
                    Slug = "menu-item"
                },
                Category = category,
                Slug = "",
                Title = title,
                Status = "published",
                Username = unitOfWork.Session.CurrentUser.Username,
                CreateDate = DateTime.Now,
                LastUpdatedDate = DateTime.Now,
                ParentID = "",
                Order = (Convert.ToInt32(order) + 1).ToString(),
                Fields = new Collection<Field>
                {
                    new Field
                    {
                        Name = "MenuPostID",
                        Value = id,
                        IsCustomField = false
                    },
                    new Field
                    {
                        Name = "Link",
                        Value = link.Replace("%23", "#"),
                        IsCustomField = false
                    }
                }
            };

            try
            {
                unitOfWork.PostRepository.Save(menuItem);
            }
            catch { return "ERROR"; }

            JavaScriptSerializer oSerializer = new JavaScriptSerializer();
            string sJSON = oSerializer.Serialize(new { MenuPostID = id, Title = title, PostID = menuItem.Id, CategorySlug = catslug, Link = link });
            return sJSON;
        }

        [HttpPost]
        [BypassAntiForgeryTokenAttribute]
        public string UpdateMenuItem(string id, string title, string menupostid, string link)
        {
            Post post = unitOfWork.PostRepository.GetByID(id);
            if (post == null) return "ERROR";

            post.Title = title;
            

            post.Fields = new List<Field>
            {
                new Field
                {
                    Name = "MenuPostID",
                    Value = menupostid,
                    IsCustomField = false
                },
                new Field
                {
                    Name = "Link",
                    Value = link.Replace("%23", "#"),
                    IsCustomField = false,
                }
            };
            unitOfWork.PostRepository.Update(post);

            JavaScriptSerializer oSerializer = new JavaScriptSerializer();
            string sJSON = oSerializer.Serialize(new { MenuPostID = menupostid, Title = post.Title, PostID = id, Link = link });
            return sJSON;
        }

        public string GetBlockFormAsString(string id, string dir)
        {
            Block block = unitOfWork.BlockRepository.GetByID(id);

            if (block == null)
            {
                block = ThemeHelpers.GetNewBlockObject(dir, id);
                if (block == null) return "";
            }
            return RenderBlockToString(block.Name + "/_Settings", block.Fields);
        }

        [BypassAntiForgeryTokenAttribute]
        public string UpdateBlocksOrder(string json)
        {
            if (String.IsNullOrWhiteSpace(json)) return "No data received. Please try again.";
            JavaScriptSerializer JSS = new JavaScriptSerializer();

            IList<Block> posts = JSS.Deserialize<IList<Block>>(json);
            if (posts.Count() > 0)
            {
                foreach (Block b in posts)
                {
                    Block block = unitOfWork.BlockRepository.GetByID(b.Id);
                    block.Order = b.Order;
                    unitOfWork.BlockRepository.Update(block);
                }
                unitOfWork.Save();
            }
            else
            {
                return "No block objects found.  Please try again.";
            }
            return "SUCCESS";
        }

        #region PartialViews

        public PartialViewResult GetConfirmDeleteMenuDialog([DefaultValue("")]string slug)
        {
            ViewBag.CategorySlug = slug;

            return PartialView("_ConfirmDeleteMenu");
        }

        public PartialViewResult GetConfirmDeleteSingleDialog([DefaultValue("")]string id, [DefaultValue("")]string ContentType)
        {
            ViewBag.PostID = id;
            ViewBag.ContentTypeID = ContentType;

            return PartialView("_ConfirmDeleteSingle");
        }

        public PartialViewResult GetConfirmDeleteUserDialog([DefaultValue("")]string id)
        {
            ViewBag.AccountID = id;

            return PartialView("_ConfirmDeleteUser");
        }

        public PartialViewResult GetConfirmDeleteCatalogDialog([DefaultValue("")]string id, [DefaultValue("")]string ContentType)
        {
            ViewBag.CategoryID = id;
            ViewBag.ContentType = ContentType;

            return PartialView("_ConfirmDeleteCategory");
        }

        public PartialViewResult GetNewAjaxCategoryView([DefaultValue("")]string id)
        {
            ViewBag.ContentTypeID = id;
            Category model = new Category();

            return PartialView("_NewAjaxCategory", model);
        }

        #endregion
    }
}