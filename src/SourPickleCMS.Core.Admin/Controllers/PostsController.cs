﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SourPickleCMS.Core.Attributes;
using SourPickleCMS.Core.Services.DataAccess;
using SourPickleCMS.Core.Models;
using System.ComponentModel;
using SourPickleCMS.Core.Models.ViewModels;
using SourPickleCMS.Core.Helpers;
using SourPickleCMS.Core.Controllers;
using System.IO;
using ImageResizer;
using System.Collections.ObjectModel;

namespace SourPickleCMS.Core.Admin.Controllers
{
    public class PostsController : SourPickleController
    {
        [RoleAuthorize(Roles = AuthRoles.Moderator)]
        public ActionResult Index([DefaultValue("")]string ContentType)
        {
            if (String.IsNullOrWhiteSpace(ContentType)) ContentType = "post";
            IList<Post> model = unitOfWork.PostRepository.GetAllByContentTypeSlug(ContentType).OrderByDescending(o => o.CreateDate).ToList();
            ViewBag.ContentType = unitOfWork.ContentTypeRepository.GetBySlug(ContentType);

            return View(model);
        }

        #region Posts

        [RoleAuthorize(Roles = AuthRoles.Writer)]
        public ActionResult Edit([DefaultValue("")]string id)
        {
            PostEditViewModel model = new PostEditViewModel();

            try
            {
                model.Post = unitOfWork.PostRepository.GetByID(id);
            }
            catch
            {
                model.Post = null;
            }
            if (model.Post == null) return RedirectToAction("Index");
            model.SaveBackup = false;
            model.Revisions = unitOfWork.PostRepository.GetPostRevisionsByParentID(model.Post.Id).OrderByDescending(o => o.CreateDate).ToList();

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [RoleAuthorize(Roles = AuthRoles.Writer)]
        public ActionResult Edit(PostEditViewModel model)
        {
            bool saveData = true;

            if (unitOfWork.Session.CurrentUser.Role < 8 && unitOfWork.Session.CurrentUser.Username != model.Post.Username)
            {
                TempData["error"] = "You do not have permission to edit this post.  Please try again.";
                return View("Error");
            }
            if (model.Post == null)
            {
                TempData["error"] = "Page data was unavailable.  Please try again.";
                saveData = false;
            }
            if (String.IsNullOrEmpty(model.Post.Title))
            {
                TempData["error"] = "No page title provided.  Please try again.";
                saveData = false;
            }
            if (String.IsNullOrEmpty(model.Post.Slug))
            {
                TempData["error"] = "No page slug provided.  Please try again.";
                saveData = false;
            }
            if (saveData)
            {
                Post tosave = unitOfWork.PostRepository.GetByID(model.Post.Id);

                tosave.LastUpdatedDate = DateTime.Now;
                if (model.Post.Category != null) tosave.Category = unitOfWork.CategoryRepository.GetBySlug(model.Post.Category.Slug, model.Post.ContentType.Slug);
                tosave.Body = model.Post.Body;
                tosave.Title = model.Post.Title;
                tosave.TemplateName = model.Post.TemplateName;
                tosave.Status = model.Post.Status;
                tosave.Order = model.Post.Order;
                tosave.Role = model.Post.Role;
                tosave.Slug = model.Post.Slug;
                tosave.TemplateName = model.Post.TemplateName;
                if (tosave.Fields != null)
                {
                    foreach (Field field in tosave.Fields.Where(f => f.IsCustomField == true))
                    {
                        model.Post.Fields.Add(field);
                    }
                }
                model.Post = tosave;
                try
                {
                    if (model.SaveBackup)
                    {
                        Post backup = model.Post.GetBackupCopy();
                        backup.Status = "revision";
                        backup.CreateDate = DateTime.Now;
                        backup.ParentID = model.Post.Id;
                        unitOfWork.PostRepository.Save(backup);
                    }

                    unitOfWork.PostRepository.Save(model.Post);
                    TempData["msg"] = "Your " + model.Post.ContentType.Slug + " was saved successfully!";
                }
                catch
                {
                    TempData["error"] = "Error saving page.  Check your input and try again.";
                }
            }

            model.Revisions = unitOfWork.PostRepository.GetPostRevisionsByParentID(model.Post.Id).OrderByDescending(o => o.CreateDate).ToList();
            return View(model);
        }

        [RoleAuthorize(Roles = AuthRoles.Writer)]
        public ActionResult New([DefaultValue("")]string ContentType)
        {
            if (String.IsNullOrWhiteSpace(ContentType)) ContentType = "blog";
            PostEditViewModel model = new PostEditViewModel
            {
                Post = new Post
                {
                    Id = "",
                    ContentType = null,
                    Status = "published"
                },
                Revisions = new List<Post>(),
            };
            model.Post.ContentType = unitOfWork.ContentTypeRepository.GetBySlug(ContentType);
            if (ContentType != "page") model.Post.Category = unitOfWork.CategoryRepository.GetBySlug("uncategorized", ContentType);

            if (model.Post.ContentType.Fields != null && model.Post.ContentType.Fields.Count() > 0)
            {
                model.Post.Fields = model.Post.ContentType.Fields;
            }

            return View("Edit", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [RoleAuthorize(Roles = AuthRoles.Writer)]
        public ActionResult New(PostEditViewModel model)
        {
            bool saveData = true;

            if (model.Post.Title == null || model.Post.Title == "")
            {
                ModelState.AddModelError("", "No page title provided.  Please try again.");
                saveData = false;
            }
            if (model.Post.Slug == null || model.Post.Slug == "")
            {
                ModelState.AddModelError("", "No page slug provided.  Please try again.");
                saveData = false;
            }

            if (saveData)
            {
                model.Post.Id = "";
                model.Post.CreateDate = DateTime.Now;
                model.Post.LastUpdatedDate = DateTime.Now;
                model.Post.Username = unitOfWork.Session.CurrentUser.Username;
                model.Post.ContentType = unitOfWork.ContentTypeRepository.GetBySlug(model.Post.ContentType.Slug);
                model.Post.Category = unitOfWork.CategoryRepository.GetBySlug("uncategorized", model.Post.ContentType.Slug);

                try
                {
                    model.Post = unitOfWork.PostRepository.Save(model.Post);

                    TempData["msg"] = model.Post.ContentType.Slug + " was saved successfully!";
                    return RedirectToAction("Index", new { ContentType = model.Post.ContentType.Slug });
                }
                catch
                {
                    ModelState.AddModelError("", "Error saving.  Check your input and try again.");
                }
            }

            model.Post.ContentType = unitOfWork.ContentTypeRepository.GetBySlug(model.Post.ContentType.Slug);
            model.Post.Category = unitOfWork.CategoryRepository.GetBySlug("uncategorized", model.Post.ContentType.Slug);

            return View("Edit", model);
        }

        [RoleAuthorize(Roles = AuthRoles.Writer)]
        public ActionResult DeletePost([DefaultValue("")]string ContentType, [DefaultValue("")]string id)
        {
            Post model = unitOfWork.PostRepository.GetByID(id);

            if (model.Username != unitOfWork.Session.CurrentUser.Username)
            {
                TempData["error"] = "You do not have permission to delete this post.";
            }
            else
            {
                if (model != null)
                {
                    if (!String.IsNullOrWhiteSpace(model.AttachedImageId))
                    {
                        Post img = unitOfWork.PostRepository.GetByID(model.AttachedImageId);
                        img.ParentID = null;
                        unitOfWork.PostRepository.Update(img);
                    }

                    unitOfWork.PostRepository.Delete(model);
                    unitOfWork.Save();
                    TempData["msg"] = "Item deleted successfully!";
                }
                else
                {
                    TempData["error"] = "Could not find the specified post.";
                }
            }
            if (ContentType == "ContentTypes-4")
            {
                return RedirectToAction("Media");
            }
            else
            {
                return RedirectToAction("Index", new { ContentType = ContentType });
            }
        }

        #endregion

        #region Categories

        [RoleAuthorize(Roles = AuthRoles.Moderator)]
        public ActionResult Categories([DefaultValue("")]string ContentType)
        {
            IList<Category> model = unitOfWork.CategoryRepository.GetByContentType(ContentType).OrderByDescending(o => o.Name).ToList();
            ViewBag.ContentType = unitOfWork.ContentTypeRepository.GetBySlug(ContentType);

            for (int x = 0; x < model.Count(); x++)
            {
                string id = model[x].Id;
                model[x].PostCount = unitOfWork.PostRepository.GetPostCountByCategory(model[x].Slug, ContentType);
            }
            return View(model);
        }

        [HttpPost]
        [RoleAuthorize(Roles = AuthRoles.Moderator)]
        public ActionResult NewCategory(Category model)
        {
            if (String.IsNullOrWhiteSpace(model.Name))
            {
                TempData["error"] = "Please enter a category name to add a new category.";
                return RedirectToAction("Categories", new { ContentType = model.ContentType });
            }
            if (String.IsNullOrWhiteSpace(model.Slug))
            {
                model.Slug = GeneralHelper.GetCategorySlug(model.Name, model.ContentType);
                if (model.Slug == "")
                {
                    TempData["error"] = "A category with this name already exists for this Content Type.";
                    return RedirectToAction("Categories", new { ContentType = model.ContentType });
                }
            }
            else
            {
                Category catcheck = unitOfWork.CategoryRepository.GetBySlug(model.Slug);
                if (catcheck != null)
                {
                    TempData["error"] = "A category with this slug already exists for this Content Type.";
                    return RedirectToAction("Categories", new { ContentType = model.ContentType });
                }
            }

            unitOfWork.CategoryRepository.Save(model);
            unitOfWork.Save();

            TempData["msg"] = "Category added successfully!";

            return RedirectToAction("Categories", new { ContentType = model.ContentType });
        }

        [RoleAuthorize(Roles = AuthRoles.Moderator)]
        public ActionResult DeleteCategory([DefaultValue("")]string id, [DefaultValue("")]string ContentType)
        {
            Category model = unitOfWork.CategoryRepository.GetByID(id);

            if (model != null)
            {
                unitOfWork.CategoryRepository.Delete(model);
                unitOfWork.Save();
                TempData["msg"] = "Category deleted successfully!";
            }
            else
            {
                TempData["error"] = "Could not find the specified category.";
            }
            return RedirectToAction("Categories", new { ContentType = ContentType });
        }

        #endregion

        #region Media

        [RoleAuthorize(Roles = AuthRoles.Writer)]
        public ActionResult Media()
        {
            IList<Post> model = unitOfWork.PostRepository.GetByContentTypeSlug("media").OrderByDescending(o => o.Id).ToList();

            return View(model);
        }

        [RoleAuthorize(Roles = AuthRoles.Writer)]
        public ActionResult EditMedia([DefaultValue("")]string id)
        {
            Post model = unitOfWork.PostRepository.GetByID(id);

            return View(model);
        }

        [HttpPost]
        [RoleAuthorize(Roles = AuthRoles.Writer)]
        public ActionResult EditMedia(Post model)
        {
            Post post = unitOfWork.PostRepository.GetByID(model.Id);
            post.Title = model.Title;
            post.AltText = model.AltText;
            post.Caption = model.Caption;
            post.Role = model.Role;
            post.Category = unitOfWork.CategoryRepository.GetBySlug(model.Category.Slug, "media");

            unitOfWork.PostRepository.Update(post);
            unitOfWork.Save();
            TempData["msg"] = "Changes saved successfully.";

            return RedirectToAction("EditMedia", new { id = model.Id });
        }

        [RoleAuthorize(Roles = AuthRoles.Writer)]
        public ActionResult NewMedia([DefaultValue("")]string id)
        {
            Post model = new Post { ParentID = id };

            return View(model);
        }

        [HttpPost]
        [RoleAuthorize(Roles = AuthRoles.Writer)]
        public ActionResult UploadMedia([DefaultValue("")]string parentid, HttpPostedFileBase file)
        {
            if (file == null || file.ContentLength < 1)
            {
                TempData["error"] = "No file specified, or the file is empty.  Please choose a file and try again.";
                return RedirectToAction("NewMedia", new { id = parentid });
            }
            string mimeType;
            String ext = Path.GetExtension(file.FileName).ToLower();

            switch (ext)
            {
                case ".png":
                    mimeType = "image/png";
                    break;
                case ".jpg":
                    mimeType = "image/jpeg";
                    break;
                case ".gif":
                    mimeType = "image/gif";
                    break;
                case ".bmp":
                    mimeType = "image/bmp";
                    break;
                case ".tif":
                    mimeType = "image/tiff";
                    break;
                default:
                    TempData["error"] = "Invalid image type.";
                    return RedirectToAction("Edit", new { id = parentid });
            }
            if (file.ContentLength / 1000 > (Convert.ToInt32(unitOfWork.Session.Settings["MaxFileUploadSize"]) * 1024))
            {
                TempData["error"] = "File exceeds maximum allowable size.  Please try again.";
                return RedirectToAction("NewMedia", new { id = parentid });
            }

            string rootPath = Request.PhysicalApplicationPath;
            DateTime createDate = DateTime.Now;
            string path = rootPath + "Uploads\\" + createDate.Year.ToString() + "\\" + createDate.Month.ToString() + "\\";
            string fname = file.FileName.ToLower().Replace(ext, "");
            fname = GeneralHelper.SlugifyText(fname);

            Directory.CreateDirectory(path);

            ImageBuilder.Current.Build(file, path + "/" + fname + "-tb" + ext, new ResizeSettings("width=" + unitOfWork.Session.Settings["ImageThumbnailWidth"] + "&height=" + unitOfWork.Session.Settings["ImageThumbnailHeight"] + "&mode=crop&anchor=middlecenter&quality=90"));
            ImageBuilder.Current.Build(file, path + "/" + fname + "-med" + ext, new ResizeSettings("maxwidth=" + unitOfWork.Session.Settings["ImageMediumMaxSize"] + "&maxheight=" + unitOfWork.Session.Settings["ImageMediumMaxSize"] + "&mode=max&scale=downscaleonly&quality=90"));
            ImageBuilder.Current.Build(file, path + "/" + fname + "-lg" + ext, new ResizeSettings("maxwidth=" + unitOfWork.Session.Settings["ImageLargeMaxSize"] + "&maxheight=" + unitOfWork.Session.Settings["ImageLargeMaxSize"] + "&mode=max&scale=downscaleonly&quality=90"));
            ImageBuilder.Current.Build(file, path + "/" + fname + ext, new ResizeSettings("quality=90"));

            Post post = new Post {
                Id = null,
                ParentID = parentid,
                MimeType = mimeType, 
                CreateDate = createDate, 
                LastUpdatedDate = createDate, 
                Slug = fname + ext,
                Username = unitOfWork.Session.CurrentUser.Username,
                Title = fname + ext,
                AltText = fname + ext, 
                Body = "", 
                Status = "published",
                Role = 1, 
                Category = unitOfWork.CategoryRepository.GetBySlug("uncategorized", "media"), 
                ContentType = unitOfWork.ContentTypeRepository.GetBySlug("media")
            };
            unitOfWork.PostRepository.Save(post);
            if (!String.IsNullOrWhiteSpace(parentid))
            {
                Post parent = unitOfWork.PostRepository.GetByID(parentid);
                parent.AttachedImageId = post.Id;
                unitOfWork.PostRepository.Save(parent);
            }
            unitOfWork.Save();

            return View("EditMedia", post);
        }

        #endregion

        #region Comments

        public ActionResult Comments()
        {
            //IEnumerable<Comment> model = unitOfWork.PostRepository();

            return View();
        }

        public ActionResult DeleteComment([DefaultValue(0)]int id)
        {
            //Comment comment = unitOfWork.CommentRepository.GetByID(id);

            //if (comment != null)
            //{
            //    unitOfWork.CommentRepository.Delete(comment);
            //    unitOfWork.Save();
            //    TempData["msg"] = "Comment deleted successfully.";
            //}
            //else
            //{
            //    TempData["error"] = "Could not find comment to delete.";
            //}

            return RedirectToAction("Comments");
        }

        #endregion
    }
}