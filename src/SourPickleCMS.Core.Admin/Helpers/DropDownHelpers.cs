﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Raven.Client;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Services.DataAccess;
using SourPickleCMS.Core.Helpers;

namespace SourPickleCMS.Core.Admin.Helpers
{
    public static class DropDownHelpers
    {
        public static HtmlString StateDropDownListFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            Dictionary<string, string> stateList = new Dictionary<string, string>()
            {
                {"AL"," Alabama"},
                {"AK"," Alaska"},
                {"AZ"," Arizona"},
                {"AR"," Arkansas"},
                {"CA"," California"},
                {"CO"," Colorado"},
                {"CT"," Connecticut"},
                {"DC"," District of Columbia"},
                {"DE"," Delaware"},
                {"FL"," Florida"},
                {"GA"," Georgia"},
                {"HI"," Hawaii"},
                {"ID"," Idaho"},
                {"IL"," Illinois"},
                {"IN"," Indiana"},
                {"IA"," Iowa"},
                {"KS"," Kansas"},
                {"KY"," Kentucky"},
                {"LA"," Louisiana"},
                {"ME"," Maine"},
                {"MD"," Maryland"},
                {"MA"," Massachusetts"},
                {"MI"," Michigan"},
                {"MN"," Minnesota"},
                {"MS"," Mississippi"},
                {"MO"," Missouri"},
                {"MT"," Montana"},
                {"NE"," Nebraska"},
                {"NV"," Nevada"},
                {"NH"," New Hampshire"},
                {"NJ"," New Jersey"},
                {"NM"," New Mexico"},
                {"NY"," New York"},
                {"NC"," North Carolina"},
                {"ND"," North Dakota"},
                {"OH"," Ohio"},
                {"OK"," Oklahoma"},
                {"OR"," Oregon"},
                {"PA"," Pennsylvania"},
                {"RI"," Rhode Island"},
                {"SC"," South Carolina"},
                {"SD"," South Dakota"},
                {"TN"," Tennessee"},
                {"TX"," Texas"},
                {"UT"," Utah"},
                {"VT"," Vermont"},
                {"VA"," Virginia"},
                {"WA"," Washington"},
                {"WV"," West Virginia"},
                {"WI"," Wisconsin"},
                {"WY"," Wyoming"}
            };
            return html.DropDownListFor(expression, new SelectList(stateList, "key", "value"));
        }

        public static HtmlString StatusDropDownListFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            Dictionary<string, string> statusList = new Dictionary<string, string>()
            {
                {"published"," Published"},
                {"pending"," Pending Approval"},
                {"draft"," Draft"}
            };
            return html.DropDownListFor(expression, new SelectList(statusList, "key", "value"));
        }

        public static HtmlString SiteModeDropDownList(this HtmlHelper html, string name, string value)
        {
            Dictionary<string, string> modeList = new Dictionary<string, string>()
            {
                {"live"," Live"},
                {"development"," Testing"},
                {"demo"," Demo"},
                {"construction"," Under Construction"},
                {"maintenance"," Down for Maintenance"}
            };
            return html.DropDownList(name, new SelectList(modeList, "key", "value", value));
        }

        public static HtmlString FieldTypesDropDownList(this HtmlHelper html, string name, string value = "", object htmlAttributes = null)
        {
            Dictionary<string, string> typeList = new Dictionary<string, string>()
            {
                {"textbox"," Textbox"},
                {"textarea"," Textarea"},
                {"checkbox"," Checkbox"},
                {"radio"," Radio Group"},
                {"dropdown"," Dropdown List"},
                {"date", " Date Picker"},
                {"datetime", " Date/Time Picker"}
            };

            return html.DropDownList(name, new SelectList(typeList, "key", "value", value), htmlAttributes);
        }

        public static HtmlString TemplateDropDownListFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            var templateFolder = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Views/Shared/Templates"));
            var themeTemplateFolder = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Themes/" + HtmlHelpers.AppSettings(null)["Theme"] + "/Views/Shared/Templates"));
            string[] templates = templateFolder.GetFiles().Select(x => x.Name).OrderBy(x => x).ToArray();
            string[] themeTemplates = themeTemplateFolder.GetFiles().Select(x => x.Name).OrderBy(x => x).ToArray();
            IEnumerable<string> tempList = templates.Union(themeTemplates);

            Dictionary<string, string> templateList = new Dictionary<string, string>();
            foreach (string temp in tempList)
            {
                templateList.Add(temp.Replace(".cshtml", ""), temp.Replace(".cshtml", "") + " Template");
            }

            return html.DropDownListFor(expression, new SelectList(templateList, "key", "value"));
        }

        public static HtmlString CategoriesDropDownListFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string ContentType)
        {
            IList<Category> categories;
            Dictionary<string, string> catList = new Dictionary<string, string>();
            using (UnitOfWork unitOfWork = new UnitOfWork((IDocumentSession)HttpContext.Current.Items["CurrentDocumentSession"]))
            {
                categories = unitOfWork.CategoryRepository.GetByContentType(ContentType).OrderBy(c => c.Id).ToList();

                foreach (Category cat in categories)
                {
                    catList.Add(cat.Slug, cat.Name);
                }
            }

            return html.DropDownListFor(expression, new SelectList(catList, "key", "value"));
        }

        public static HtmlString CategoriesDropDownList(this HtmlHelper html, string name, string ContentType)
        {
            IEnumerable<Category> categories;
            Dictionary<string, string> catList = new Dictionary<string, string>();
            using (UnitOfWork unitOfWork = new UnitOfWork((IDocumentSession)HttpContext.Current.Items["CurrentDocumentSession"]))
            {
                categories = unitOfWork.CategoryRepository.GetByContentType("menu-item").OrderBy(c => c.Id).ToList();

                foreach (Category cat in categories)
                {
                    catList.Add(cat.Slug, cat.Name);
                }
            }

            return html.DropDownList(name, new SelectList(catList, "key", "value"));
        }

        public static HtmlString RolesDropDownList(this HtmlHelper html, string name, string value)
        {
            Dictionary<string, string> modeList = new Dictionary<string, string>()
            {
                {"0"," Guest"},
                {"2"," User"},
                {"4"," Writer"},
                {"6"," Publisher"},
                {"8"," Moderator"},
                {"10"," Admin"}
            };
            return html.DropDownList(name, new SelectList(modeList, "key", "value", value));
        }

        public static HtmlString CustomFieldsDropDownList(this HtmlHelper html, string name, object htmlAttrs, string ptslug)
        {
            Dictionary<string, string> fieldList = new Dictionary<string, string>();
            using (UnitOfWork unitOfWork = new UnitOfWork((IDocumentSession)HttpContext.Current.Items["CurrentDocumentSession"]))
            {
                ContentType pt = unitOfWork.ContentTypeRepository.GetBySlug(ptslug);
                fieldList.Add("", "Select a Name");

                if (pt.Fields != null)
                {
                    foreach (Field field in pt.Fields)
                    {
                        fieldList.Add(field.Name, field.Name);
                    }
                }
            }

            return html.DropDownList(name, new SelectList(fieldList, "key", "value"), htmlAttrs);
        }

        public static HtmlString StyleFrameworksDropDownList(this HtmlHelper html, string name, string value)
        {
            Dictionary<string, string> stylesList = new Dictionary<string, string>()
            {
                {"none"," None"},
                {"1140"," 1140 Grid"},
                {"960_12"," 960 Grid - 12 Columns"},
                {"960_16"," 960 Grid - 16 Columns"},
                {"960_24"," 960 Grid - 24 Columns"},
                {"blueprint"," Blueprint System"},
                {"skeleton"," Skeleton"}
            };
            return html.DropDownList(name, new SelectList(stylesList, "key", "value", value));
        }
    }
}