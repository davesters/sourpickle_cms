﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Services.DataAccess;
using Raven.Client;
using System.IO;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Reflection;

namespace SourPickleCMS.Core.Admin.Helpers
{
    public static class AdminHelpers
    {
        public static HtmlString RenderContentTypeMenus(this HtmlHelper helper)
        {
            IList<ContentType> types = new List<ContentType>();

            using (UnitOfWork unitOfWork = new UnitOfWork((IDocumentSession)HttpContext.Current.Items["CurrentDocumentSession"]))
            {
                types = unitOfWork.ContentTypeRepository.GetCustomContentTypes();
                string output = "";

                foreach (ContentType ct in types)
                {
                    string plural = ct.PluralName.ToLower().Replace(' ', '_');

                    output += "<li name='" + plural + "'><a href='javascript://'><img src='/Areas/Admin/Content/Images/icons/menu/pages.png' alt='' /> " + ct.PluralName + "</a>";
                    output += "<ul>";
                    output += "<li name='all_" + ct.Slug + "'><a href='/Admin/Posts/Index/" + ct.Slug + "'>All " + ct.PluralName + "</a></li>";
                    output += "<li name='new_" + ct.Slug + "'><a href='/Admin/Posts/New/" + ct.Slug + "'>New " + ct.Name + "</a></li>";
                    if (ct.Commentable)
                    {
                        output += "<li name='" + ct.Slug + "_categories'><a href='/Admin/Posts/Categories?ContentType=" + ct.Slug + "'>Categories</a></li>";
                    }
                    output += "</ul></li>";
                }

                return new HtmlString(output);
            }
        }

        public static HtmlString RenderCustomMenus(this HtmlHelper helper)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(null))
            {
                IList<CustomMenu> menus = unitOfWork.Session.CustomMenus;
                if (menus == null)
                {
                    menus = GetCustomMenus(helper.ViewContext.Controller.ControllerContext.HttpContext.Server.MapPath("~/"), unitOfWork.Session.Settings["Theme"]);
                    if (menus == null) return null; //menus = new List<CustomMenu>();
                    unitOfWork.Session.CustomMenus = menus;
                }
                string output = "";

                foreach (CustomMenu m in menus)
                {
                    string iconurl = (!string.IsNullOrWhiteSpace(m.IconUrl)) ? m.IconUrl : "/Areas/Admin/Content/Images/icons/menu/page.png";
                    output += "<li name='" + m.Slug + "'><a href='javascript://'><img src='" + iconurl + "' alt='' /> " + m.Title + "</a>";
                    output += "<ul>";
                    foreach (CustomMenuItem mi in m.MenuItems)
                    {
                        output += "<li name='" + mi.Slug + "'><a href='/Admin/" + mi.Controller + "/" + mi.Action + mi.QueryString + "'>" + mi.Name + "</a></li>";
                    }
                    output += "</ul></li>";
                }

                return new HtmlString(output);
            }
        }

        public static IList<CustomMenu> GetCustomMenus(string dir, string theme)
        {
            Dictionary<string, string> providerOptions = new Dictionary<string, string>
            {
                {"CompilerVersion", "v4.0"}
            };

            CSharpCodeProvider provider = new CSharpCodeProvider(providerOptions);

            CompilerParameters cp = new CompilerParameters
            {
                GenerateInMemory = true,
                GenerateExecutable = false
            };
            cp.ReferencedAssemblies.Add("System.dll");
            cp.ReferencedAssemblies.Add(dir + "bin\\SourPickleCMS.Core.dll");

            string fname = dir + "Themes\\" + theme + "\\CustomCode.cs";
            try
            {
                CompilerResults results = provider.CompileAssemblyFromFile(cp, fname);

                Object o = results.CompiledAssembly.CreateInstance("CustomCode");
                MethodInfo mi = o.GetType().GetMethod("CustomMenu");
                return mi.Invoke(o, null) as List<CustomMenu>;
            }
            catch
            {
                return null;
            }
        }
    }
}