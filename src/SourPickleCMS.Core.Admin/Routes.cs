﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SourPickleCMS.Core.Admin
{
    public class Routes : AreaRegistration
    {
        public override string AreaName
        {
            get { return "Admin"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AdminHome",
                "Admin",
                new { controller = "Main", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "AdminHome2",
                "Admin/Home",
                new { controller = "Main", action = "Index", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "AdminPostsIndex",
                "Admin/Posts/Index/{ContentType}",
                new { controller = "Posts", action = "Index", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "AdminNewPost",
                "Admin/Posts/New/{ContentType}",
                new { controller = "Posts", action = "New", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "Admin",
                "Admin/{controller}/{action}/{id}",
                new { controller = "Main", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}