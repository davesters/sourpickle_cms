﻿* Block Info

Title: Category List
Author: David Corona
Website: http://www.sourpickle-cms.com
Description: Display a list of the all categories for a certain content type.
Fields: ContentType