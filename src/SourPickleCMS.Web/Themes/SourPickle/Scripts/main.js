﻿$(document).ready(function () {
    $('#text_slider').cycle({
        fx: 'scrollVert',
        prev: '.slideshow_left_button',
        next: '.slideshow_right_button',
        timeout: 8000
    });
    $('#images_slider').cycle({
        fx: 'scrollHorz',
        prev: '.slideshow_left_button',
        next: '.slideshow_right_button',
        timeout: 8000
    });
});