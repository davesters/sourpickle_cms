﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SourPickleCMS.Core;

namespace SourPickleCMS.Web
{
    public class MvcApplication : SourPickleApplication
    {
        protected MvcApplication() : base() {
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            // Enter your custom routing here
        }

        protected override void Application_Start()
        {
            RegisterRoutes(RouteTable.Routes);

            base.Application_Start();

            // Add your custom indexes here after calling the base Application_Start.
            // Note: You only need to register one index in your project and the rest will be added automatically.
            // Example: RegisterIndex<Classname>();
        }
    }
}