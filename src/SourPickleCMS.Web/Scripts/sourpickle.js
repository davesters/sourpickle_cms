﻿$(document).ready(function () {

    // Setup event listeners if elements exist

    if ($(".reply_link").length > 0) {
        $(".reply_link").bind("click", function () {
            var id = $(this).attr("id").replace("reply_link_", "");
            var reply_box = $("#comment_box_" + id).find(".comment_reply_box");
            var form_box = $("#comment_container").parent();
            $(form_box).slideUp('fast');
            $(reply_box).append($("#comment_container")).slideDown('fast');
            $("#Parent").val(id);
            $("#cancel_link").css("display", "inline-block");
        });
    }
});

function cancel_comment_reply() {
    $("#Parent").val(0);
    $("#comments_form").append($("#comment_container"));
    $("#cancel_link").css("display", "none");
    $("#comments_form").slideDown('fast');
}