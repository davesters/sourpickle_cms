﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SourPickleCMS.Core.Services.DataAccess;

namespace SourPickleCMS.Core.Extensions
{
    public class CustomViewEngine : IViewEngine
    {
        private string lastTheme;
        private RazorViewEngine lastEngine;
        private readonly object @lock = new object();

        private RazorViewEngine CreateRealViewEngine()
        {
            IDictionary<string, string> settings = new Dictionary<string, string>();

            using (UnitOfWork unitOfWork = new UnitOfWork(null))
            {
                settings = unitOfWork.Session.Settings;
            }

            lock (@lock)
            {
                try
                {
                    if (settings["Theme"] == lastTheme)
                    {
                        return lastEngine;
                    }
                }
                catch (Exception)
                {
                    return new RazorViewEngine();
                }

                // Create a new razor view engine using the theme name when prioritizing names for resolving views
                lastEngine = new RazorViewEngine();

                lastEngine.PartialViewLocationFormats =
                    new[]
                    {
                        "~/Themes/" + settings["Theme"] + "/Views/Shared/PartialViews/{0}.cshtml",
                        "~/Views/Shared/PartialViews/{0}.cshtml",
                        "~/Areas/Admin/Views/Shared/PartialViews/{0}.cshtml",
                        "~/Areas/Admin/Blocks/{0}.cshtml",
                        "~/Blocks/{0}.cshtml"
                    }.Union(lastEngine.PartialViewLocationFormats).ToArray();

                lastEngine.ViewLocationFormats =
                    new[]
                    {
                        "~/Themes/" + settings["Theme"] + "/Views/{1}/{0}.cshtml",
                        "~/Themes/" + settings["Theme"] + "/Views/Shared/{0}.cshtml",
                        "~/Themes/" + settings["Theme"] + "/Views/Shared/Templates/{0}.cshtml",
                        "~/Views/{1}/{0}.cshtml",
                        "~/Views/Shared/{0}.cshtml",
                        "~/Views/Shared/Templates/{0}.cshtml",
                    }.Union(lastEngine.ViewLocationFormats).ToArray();

                lastEngine.MasterLocationFormats =
                    new[]
                    {
                        "~/Themes/" + settings["Theme"] + "/Views/Shared/{0}.cshtml",
                        "~/Views/Shared/{0}.cshtml"
                    }.Union(lastEngine.MasterLocationFormats).ToArray();

                lastTheme = settings["Theme"];

                return lastEngine;
            }
        }

        public ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName, bool useCache)
        {
            return CreateRealViewEngine().FindPartialView(controllerContext, partialViewName, useCache);
        }

        public ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache)
        {
            ViewEngineResult res = CreateRealViewEngine().FindView(controllerContext, viewName, masterName, useCache);
            if (res.View == null)
            {
                res = CreateRealViewEngine().FindView(controllerContext, "Default", masterName, useCache);
            }
            return res;
        }

        public void ReleaseView(ControllerContext controllerContext, IView view)
        {
            CreateRealViewEngine().ReleaseView(controllerContext, view);
        }
    }
}