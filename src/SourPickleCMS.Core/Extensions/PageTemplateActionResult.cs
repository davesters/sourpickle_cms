﻿using System.Web.Mvc;

namespace SourPickleCMS.Core.Extensions
{
    public class PageTemplateActionResult : ActionResult
    {
        private readonly string _pageTemplate;

        public PageTemplateActionResult(string pageTemplate = null)
        {
            _pageTemplate = (pageTemplate ?? "Default");
        }

        public override void ExecuteResult(ControllerContext context)
        {
            new ViewResult
                {
                    ViewData = context.Controller.ViewData,
                    ViewName = _pageTemplate,
                    TempData = context.Controller.TempData,
                }.ExecuteResult(context);
        }
    }
}