﻿using System.Web.Mvc;
using SourPickleCMS.Core.Services.DataAccess;
using System.Collections.Generic;

namespace SourPickleCMS.Core.Application.Extensions
{
    public static class ViewDataDictionaryExtensions
    {
        public static bool IsLoggedIn(this ViewDataDictionary viewData)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(null))
            {
                return unitOfWork.Session.LoggedIn;
            }
        }

        public static IDictionary<string, string> AppSettings(this ViewDataDictionary viewData)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(null))
            {
                return unitOfWork.Session.Settings;
            }
        }
    }
}
