﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SourPickleCMS.Core.Models;
using System.IO;

namespace SourPickleCMS.Core.Helpers
{
    public static class ThemeHelpers
    {
        public static Block GetNewBlockObject(string dir, string name)
        {
            Block block = new Block();
            var folder = new DirectoryInfo(dir);

            StreamReader sr = null;
            try
            {
                sr = new StreamReader(folder.FullName + "/" + name + "/BlockInfo.txt");
            }
            catch
            {
                return null;
            }
            block.Fields = new Dictionary<string, string>();
            block.Name = name;
            block.Directory = folder.FullName;

            do
            {
                string line = sr.ReadLine();
                if (String.IsNullOrWhiteSpace(line) == true || line.Substring(0, 1) == "*") continue;
                string key = line.Substring(0, line.IndexOf(':')).Trim();
                string value = line.Substring(line.IndexOf(':') + 1).Trim();
                switch (key)
                {
                    case "Title":
                        block.Title = value;
                        break;
                    case "Author":
                        block.Author = value;
                        break;
                    case "Website":
                        block.Website = value;
                        break;
                    case "Description":
                        block.Description = value;
                        break;
                    case "Fields":
                        foreach (String s in value.Split(','))
                        {
                            block.Fields.Add(s.Trim(), "");
                        }
                        block.Fields.Add("Title", block.Title);
                        block.Fields.Add("BlockName", block.Name);
                        break;
                }
            } while (!sr.EndOfStream);

            sr.Close();
            return block;
        }

        public static IList<Block> GetBlocks(string dir)
        {
            IList<Block> blocks = new List<Block>();
            var folder = new DirectoryInfo(dir);
            string[] folders = folder.GetDirectories("*", SearchOption.TopDirectoryOnly).Select(x => x.Name).OrderBy(x => x).ToArray();

            foreach (string name in folders)
            {
                Block block = new Block();
                StreamReader sr = null;
                try
                {
                    sr = new StreamReader(dir + "/" + name + "/BlockInfo.txt");
                }
                catch
                {
                    sr.Close();
                    continue;
                }
                block.Name = name;
                block.Fields = new Dictionary<string, string>();
                block.Order = "";
                block.Directory = folder.FullName;

                do
                {
                    string line = sr.ReadLine();
                    if (String.IsNullOrWhiteSpace(line) == true || line.Substring(0, 1) == "*") continue;
                    string key = line.Substring(0, line.IndexOf(':')).Trim();
                    string value = line.Substring(line.IndexOf(':') + 1).Trim();
                    switch (key)
                    {
                        case "Title":
                            block.Title = value;
                            break;
                        case "Author":
                            block.Author = value;
                            break;
                        case "Website":
                            block.Website = value;
                            break;
                        case "Description":
                            block.Description = value;
                            break;
                        case "Fields":
                            foreach (String s in value.Split(','))
                            {
                                block.Fields.Add(s, "");
                            }
                            break;
                    }
                } while (!sr.EndOfStream);

                blocks.Add(block);
                sr.Close();
            }
            return blocks;
        }

        public static IList<string> GetRegions(string dir)
        {
            IList<string> regions = new List<string>();
            var folder = new DirectoryInfo(dir);

            StreamReader sr = null;
            try
            {
                sr = new StreamReader(folder + "/ThemeInfo.txt");
            }
            catch
            {
                sr.Close();
                return null;
            }

            do
            {
                string line = sr.ReadLine();
                if (String.IsNullOrWhiteSpace(line) == true || line.Substring(0, 1) == "*") continue;
                string key = line.Substring(0, line.IndexOf(':')).Trim();
                string value = line.Substring(line.IndexOf(':') + 1).Trim();

                if (key == "Regions")
                {
                    foreach (string s in value.Split(','))
                    {
                        regions.Add(s.Trim());
                    }
                }
            } while (!sr.EndOfStream);
            sr.Close();
            return regions;
        }
    }
}
