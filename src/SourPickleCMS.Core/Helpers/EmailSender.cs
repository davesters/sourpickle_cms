﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using SourPickleCMS.Core.Models;
using System.Net;

namespace SourPickleCMS.Core.Helpers
{
    public class EmailSender : IDisposable
    {
        private readonly SmtpClient _smtp;
        private EmailSettings _settings;
        private MailMessage mail;

        public EmailSender(EmailSettings settings)
        {
            _settings = settings;
            _smtp = new SmtpClient
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = _settings.UseSSL,
                Port = _settings.Port,
                Host = _settings.Host,
            };
            if (_settings.UseAuthentication)
            {
                _smtp.Credentials = new NetworkCredential(_settings.Username, _settings.Password);
            }
            mail = new MailMessage
            {
                From = new MailAddress(_settings.FromAddress, _settings.FromName),
                Subject = _settings.Subject,
            };
            mail.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(WebUtility.HtmlDecode(settings.Body), null, "text/html"));
            foreach (string to in _settings.To.Split(','))
            {
                if (String.IsNullOrWhiteSpace(to)) continue;
                mail.To.Add(to);
            }
        }

        public void Send()
        {
            _smtp.Send(mail);
        }

        public void SendAsync()
        {
            _smtp.SendCompleted += new SendCompletedEventHandler(AsyncSendCompleted);
            _smtp.SendAsync(mail, mail);
        }

        private void AsyncSendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            _smtp.SendCompleted -= AsyncSendCompleted;
        }

        public void Dispose()
        {
            _smtp.Dispose();
        }
    }
}