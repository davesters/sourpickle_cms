﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Raven.Client;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Services.DataAccess;
using SourPickleCMS.Core.Controllers;
using System.Web.WebPages;

namespace SourPickleCMS.Core.Helpers
{
    public static class HtmlHelpers
    {
        public static HtmlString TruncateString(this HtmlHelper helper, string content, int max_length, string ellipsis = "[...]")
        {
            if (content.Length <= max_length) return new HtmlString(content);

            content = content.Substring(0, max_length) + " " + ellipsis;
            return new HtmlString(content);
        }

        public static HtmlString RenderPrettyDate(this HtmlHelper helper, DateTime date)
        {
            return new HtmlString(GeneralHelper.GetPrettyDate(date));
        }

        public static HtmlString StyleSheets(this HtmlHelper helper)
        {
            IDictionary<string, string> settings;
            string path = helper.ViewContext.HttpContext.Request.PhysicalApplicationPath;

            using (UnitOfWork unitOfWork = new UnitOfWork(null))
            {
                settings = unitOfWork.Session.Settings;
            }
            string styles = "";

            switch (settings["StyleFramework"])
            {
                case "1140":
                    styles += "<link rel='stylesheet' href='/Content/Css/1140/1140.css' >";
                    break;
                case "960_12":
                    styles += "<link rel='stylesheet' href='/Content/Css/960/960_12_col.css' >";
                    break;
                case "960_16":
                    styles += "<link rel='stylesheet' href='/Content/Css/960/960_16_col.css' >";
                    break;
                case "960_24":
                    styles += "<link rel='stylesheet' href='/Content/Css/960/960_24_col.css' >";
                    break;
                case "blueprint":
                    styles += "<link rel='stylesheet' href='/Content/Css/blueprint/screen.css' >";
                    break;
                case "skeleton":
                    styles += "<link rel='stylesheet' href='/Content/Css/skeleton/skeleton.css' >";
                    break;
            }
            if (File.Exists(path + "/Themes/" + settings["Theme"] + "/Content/Css/Styles.css"))
            {
                styles += "<link rel='stylesheet' href='/Themes/" + settings["Theme"] + "/Content/Css/Styles.css' >";
            }
            else if (File.Exists(path + "/Content/Css/Styles.css"))
            {
                styles += "<link rel='stylesheet' href='/Content/Css/Styles.css' >";
            }
            if (!String.IsNullOrWhiteSpace(settings["CustomStyles"]))
            {
                foreach (string s in settings["CustomStyles"].Split(','))
                {
                    if (File.Exists(path + "/Themes/" + settings["Theme"] + "/Content/Css/" + s))
                    {
                        styles += "<link rel='stylesheet' href='/Themes/" + settings["Theme"] + "/Content/Css/" + s + "' >";
                    }
                }
            }
            return new HtmlString(styles);
        }

        public static HtmlString Scripts(this HtmlHelper helper)
        {
            IDictionary<string, string> settings;
            string path = helper.ViewContext.HttpContext.Request.PhysicalApplicationPath;

            using (UnitOfWork unitOfWork = new UnitOfWork(null))
            {
                settings = unitOfWork.Session.Settings;
            }
            string scripts = "";

            if (settings["IncludeJQuery"] == "true")
            {
                scripts += "<script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js\"></script>";
                scripts += "<script type=\"text/javascript\">if (typeof jQuery == 'undefined'){document.write(unescape(\"%3Cscript src='/Scripts/Libs/jquery-1.7.1.min.js' type='text/javascript'%3E%3C/script%3E\"));}</script>";
            }
            if (settings["AjaxInComments"] == "true") scripts += "<script src='/Scripts/sourpickle.js'></script>";

            if (!String.IsNullOrWhiteSpace(settings["CustomScripts"]))
            {
                foreach (string s in settings["CustomScripts"].Split(','))
                {
                    if (File.Exists(path + "/Themes/" + settings["Theme"] + "/Scripts/" + s.Trim()))
                    {
                        scripts += "<script src='/Themes/" + settings["Theme"] + "/Scripts/" + s.Trim() + "'></script>";
                    }
                }
            }
            if (settings.ContainsKey("Scripts")) scripts += settings["Scripts"];
            return new HtmlString(scripts);
        }

        public static IDictionary<string, string> AppSettings(this HtmlHelper helper)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(null))
            {
                return unitOfWork.Session.Settings;
            }
        }

        public static Account User(this HtmlHelper helper)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(null))
            {
                return unitOfWork.Session.CurrentUser;
            }
        }

        public static string ThemePath(this HtmlHelper helper)
        {
            return "/Themes/" + helper.AppSettings()["Theme"];
        }

        public static void AddScript(this HtmlHelper helper, string script)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(null))
            {
                if (!unitOfWork.Session.Settings.ContainsKey("Scripts")) unitOfWork.Session.Settings.Add("Scripts", "");

                unitOfWork.Session.Settings["Scripts"] += "<script type='text/javascript' src='" + script + "'></script>";
            }
        }

        public static HtmlString RenderMenu(this HtmlHelper helper, string menu_slug)
        {
            string output = "";
            IList<Post> posts;
            string curr_page = helper.ViewContext.HttpContext.Request.RawUrl;

            using (UnitOfWork unitOfWork = new UnitOfWork((IDocumentSession)HttpContext.Current.Items["CurrentDocumentSession"]))
            {
                posts = unitOfWork.PostRepository.GetPostsByCategorySlug(menu_slug).OrderBy(o => o.Order).ToList();
                if (posts == null || posts.Count() == 0) return new HtmlString("");

                output += "<nav class=\"nav-menu\"><ul id =\"menu-" + menu_slug + "\" class=\"parent-menu\">";

                foreach (Post p in posts)
                {
                    string slug = "";
                    bool is_curr_page = false;
                    string link = "";

                    if (p.Values.ContainsKey("Link")) link = p.Values["Link"];

                    if (!String.IsNullOrWhiteSpace(link))
                    {
                        slug = link;
                    }
                    else
                    {
                        slug = "/";
                    }

                    if (curr_page == slug)
                    {
                        is_curr_page = true;
                    }
                    else if (((curr_page == "/" || curr_page == "/home") && (slug == "/home")))
                    {
                        is_curr_page = true;
                    }
                    if (slug == "/home") slug = "/";

                    output += "<li id=\"menu-item-" + p.Id + "\" class=\"menu-item\" >";
                    output += "<a ";
                    if (is_curr_page) output += "class=\"current-page\" ";
                    output += "href=\"" + slug + "\">" + p.Title + "</a>";
                    output += "</li>";
                }
            }
            output += "</ul></nav>";
            return new HtmlString(output);
        }

        public static HtmlString Region(this HtmlHelper helper, string region)
        {
            string output = "";

            using (UnitOfWork unitOfWork = new UnitOfWork((IDocumentSession)HttpContext.Current.Items["CurrentDocumentSession"]))
            {
                IList<Block> blocks = unitOfWork.BlockRepository.GetBlocksByRegion(region, unitOfWork.Session.Settings["Theme"]).OrderBy(o => o.Order).ToList();

                if (blocks.Count > 0)
                {
                    foreach (Block b in blocks)
                    {
                            using (StringWriter sw = new StringWriter())
                            {
                                helper.ViewContext.ViewData.Model = b;
                                var context = new ControllerContext(helper.ViewContext.HttpContext, helper.ViewContext.RouteData, helper.ViewContext.Controller);
                                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(context, b.Name + "/_" + b.Name);
                                ViewContext viewContext = new ViewContext(context, viewResult.View, helper.ViewContext.ViewData, helper.ViewContext.TempData, sw);
                                viewResult.View.Render(viewContext, sw);

                                output += sw.GetStringBuilder().ToString();
                            }
                    }
                }
                else
                {
                    return new HtmlString("");
                }
            }
            return new HtmlString(output);
        }

        public static HtmlString GetBlock(this HtmlHelper helper, string block)
        {
            return new HtmlString("Block goes here...");
        }

        public static HtmlString GetComments(this HtmlHelper helper)
        {
            Post post = PostsHelper.GetPost(helper, "");
            string ret = "";

            if (post.Comments == null || post.Comments.Count() == 0)
            {
                ret += "<h3>There are no comments yet.  Be the first to add a comment!</h3>";
                if (post.CommentsClosed) ret += "<h3>Comments have been closed for this post</h3>";
            }
            else
            {
                if (post.Comments.Count() == 1)
                {
                    ret += "<h3>There is 1 comment on " + post.Title + "</h3><ul id='comments'>";
                }
                else
                {
                    ret += "<h3>There are " + post.Comments.Count() + " comments on " + post.Title + "</h3><ul id='comments'>";
                }

                foreach (Comment comment in post.Comments)
                {
                    ret += GeneralHelper.RenderComment(comment);
                }

                ret += "</ul>"; //<-- Close Comments div
            }
            return new HtmlString(ret);
        }

        public static HtmlString GetBodyClasses(this HtmlHelper helper)
        {
            Post post = PostsHelper.GetPost(helper);
            string classes = "";

            if (!String.IsNullOrWhiteSpace(post.Title))
            {
                classes += post.Slug + " ";
                if (post.ContentType != null) classes += post.ContentType.Slug + " ";
            }

            foreach (string path in helper.ViewContext.RequestContext.HttpContext.Request.Url.LocalPath.Split('/'))
            {
                if (!String.IsNullOrWhiteSpace(path)) classes += path + " ";
            }

            return new HtmlString(classes);
        }

        public static HtmlString SP_Head(this HtmlHelper helper)
        {
            // Reserved for future use for plugins and themes.  Right now does nothing

            return new HtmlString("");
        }

        public static HtmlString SP_Footer(this HtmlHelper helper)
        {
            // Right now this only renders the toolbar if necessary.
            // Future use may give more options for plugins or themes

            string admin_toolbar = "";

            using (UnitOfWork unitOfWork = new UnitOfWork(null))
            {
                if (unitOfWork.Session.CurrentUser.Role > 2)
                {
                    admin_toolbar += "<link rel='stylesheet' href='/Content/Css/AdminToolbar.css' >";
                    admin_toolbar += "<script src='/Scripts/AdminToolbar.js'></script>";

                    admin_toolbar += "<div id='admin_toolbar_container'>";
                    admin_toolbar += "<div class='left'>";
                    admin_toolbar += "<a href='http://www.sourpickle-cms.com'>Sour Pickle CMS</a> |";
                    admin_toolbar += "<a href='javascript://'>Add New</a>";

                    admin_toolbar += "</div><div class='right'>Hi, <a href='/User/Profile/" + unitOfWork.Session.CurrentUser.Username + "' >" + unitOfWork.Session.CurrentUser.Username + "</a> |";
                    admin_toolbar += "<a href='/User/LogOff'>Logout</a> |";
                    admin_toolbar += "<a href='/Admin'>Dashboard</a>";
                    admin_toolbar += "</div></div>";
                }
            }

            return new HtmlString("");
        }
    }
}