﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Web;
using Raven.Client;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Services.DataAccess;

namespace SourPickleCMS.Core.Helpers
{
    public static class GeneralHelper
    {
        public static string GetCategorySlug(this string phrase, string ContentType)
        {
            string slug = "";

            using (UnitOfWork unitOfWork = new UnitOfWork((IDocumentSession)HttpContext.Current.Items["CurrentDocumentSession"]))
            {
                slug = SlugifyText(phrase);

                Category test = unitOfWork.CategoryRepository.GetBySlug(slug, ContentType);
                if (test != null) return "";

                return slug;
            }
        }

        public static string GetUniqueSlug(this string phrase, string catslug, string ContentType)
        {
            string slug = "";

            using (UnitOfWork unitOfWork = new UnitOfWork((IDocumentSession)HttpContext.Current.Items["CurrentDocumentSession"]))
            {
                slug = SlugifyText(phrase);

                int counter = 1;
                bool done = false;
                string tempslug = slug;
                do
                {
                    Post test = new Post();
                    if (ContentType == "page")
                    {
                        test = unitOfWork.PostRepository.GetBySlugAndType(tempslug, ContentType);
                    }
                    else
                    {
                        test = unitOfWork.PostRepository.GetPostBySlugByTypeAndCategory(tempslug, catslug, ContentType);
                    }

                    if (test != null)
                    {
                        counter++;
                        tempslug = slug + "-" + counter.ToString();
                    }
                    else
                    {
                        done = true;
                        slug = tempslug;
                    }
                } while (!done);
            }

            return slug;
        }

        public static string SlugifyText(string txt)
        {
            string slug = txt.RemoveAccent().ToLower();
            slug = Regex.Replace(slug, @"[^a-z0-9\s-]", ""); // invalid chars
            slug = Regex.Replace(slug, @"\s+", " ").Trim(); // convert multiple spaces into one space
            slug = slug.Substring(0, slug.Length <= 62 ? slug.Length : 62).Trim(); // cut and trim it
            slug = Regex.Replace(slug, @"\s", "-"); // hyphens
            slug = slug.Replace("--", "-");
            slug = slug.Replace("---", "-");
            slug = slug.Replace("----", "-");

            return slug;
        }

        public static string RemoveAccent(this string txt)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }

        public static string CheckSlug(this string slug, string catslug, string ContentType)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork((IDocumentSession)HttpContext.Current.Items["CurrentDocumentSession"]))
            {
                int counter = 1;
                bool done = false;
                string tempslug = slug;
                do
                {
                    Post test = new Post();
                    if (ContentType == "page")
                    {
                        test = unitOfWork.PostRepository.GetBySlugAndType(tempslug, ContentType);
                    }
                    else
                    {
                        test = unitOfWork.PostRepository.GetPostBySlugByTypeAndCategory(tempslug, catslug, ContentType);
                    }

                    if (test != null)
                    {
                        counter++;
                        tempslug = slug + "-" + counter.ToString();
                    }
                    else
                    {
                        done = true;
                        slug = tempslug;
                    }
                } while (!done);
            }

            return slug;
        }

        public static Dictionary<string, string> ToDictionary(this object @object)
        {
            if (@object == null)
            {
                return new Dictionary<string, string>();
            }

            var properties = TypeDescriptor.GetProperties(@object);

            var hash = new Dictionary<string, string>(properties.Count);

            foreach (PropertyDescriptor descriptor in properties)
            {
                var key = descriptor.Name;
                var value = descriptor.GetValue(@object).ToString();

                if (!String.IsNullOrWhiteSpace(value))
                {
                    hash.Add(key, value);
                }
            }

            return hash;
        }

        public static string GetPrettyDate(DateTime d)
        {
            // 1.
            // Get time span elapsed since the date.
            TimeSpan s = DateTime.Now.Subtract(d);

            // 2.
            // Get total number of days elapsed.
            int dayDiff = (int)s.TotalDays;

            // 3.
            // Get total number of seconds elapsed.
            int secDiff = (int)s.TotalSeconds;

            // 4.
            // Don't allow out of range values.
            if (dayDiff < 0)
            {
                return null;
            }

            // 5.
            // Handle same-day times.
            if (dayDiff == 0)
            {
                // A.
                // Less than one minute ago.
                if (secDiff < 60)
                {
                    return "just now";
                }
                // B.
                // Less than 2 minutes ago.
                if (secDiff < 120)
                {
                    return "1 minute ago";
                }
                // C.
                // Less than one hour ago.
                if (secDiff < 3600)
                {
                    return string.Format("{0} minutes ago",
                        Math.Floor((double)secDiff / 60));
                }
                // D.
                // Less than 2 hours ago.
                if (secDiff < 7200)
                {
                    return "1 hour ago";
                }
                // E.
                // Less than one day ago.
                if (secDiff < 86400)
                {
                    return string.Format("{0} hours ago",
                        Math.Floor((double)secDiff / 3600));
                }
            }
            // 6.
            // Handle previous days.
            if (dayDiff == 1)
            {
                return "yesterday";
            }
            if (dayDiff < 7)
            {
                return string.Format("{0} days ago",
                dayDiff);
            }
            if (dayDiff < 31)
            {
                return string.Format("{0} weeks ago",
                Math.Ceiling((double)dayDiff / 7));
            }
            if (dayDiff < 365)
            {
                return string.Format("{0} months ago",
                Math.Ceiling((double)dayDiff / 30));
            }
            if (dayDiff == 365)
            {
                return "1 year ago";
            }
            if (dayDiff > 365 && dayDiff < 720)
            {
                return "over 1 year ago";
            }
            if (dayDiff > 720)
            {
                return string.Format("over {0} years ago",
                Math.Floor((double)dayDiff / 365));
            }
            return null;
        }

        public static string RenderComment(Comment comment)
        {
            string ret = "";
            int reply_count = comment.Order.Split('.').Length - 1;

            if (!String.IsNullOrWhiteSpace(comment.AuthorUrl)) comment.AuthorUrl = "http://" + comment.AuthorUrl.Replace("http://", "");
            ret += "<li class='comment_box' id='comment_box_" + comment.Id + "' data-order='" + comment.Order + "' style='margin-left: " + (reply_count * 40).ToString() + "px'>";

            ret += "<div class='avatar'>" + HtmlHelperGravatar.Gravatar(null, comment.AuthorEmail, 40, GravatarRating.Default, GravatarDefaultImage.Identicon);
            ret += "</div>";

            ret += "<div class='body'>";
            ret += "<div class='name'>";
            if (!String.IsNullOrWhiteSpace(comment.AuthorUrl)) ret += "<a href='" + comment.AuthorUrl + "' target='_blank'>";
            ret += "<strong>" + comment.Username + "</strong>";
            if (!String.IsNullOrWhiteSpace(comment.AuthorUrl)) ret += "</a>";
            ret += " replies:</div>";
            ret += "<div class='date'>" + GeneralHelper.GetPrettyDate(comment.CreateDate) + " (" + comment.CreateDate.ToShortDateString() + " " + comment.CreateDate.ToShortTimeString() + ")</div>";
            ret += "<div class='message'>" + comment.Message.Replace("\r\n", "<br />").Replace("\n", "<br />") + "<br /><br />";
            if (reply_count < 8)
            {
                ret += "<a class='reply_link' id='reply_link_" + comment.Id + "' href='javascript://'>reply</a></div>";
                ret += "<div class='comment_reply_box'></div>";
            }
            ret += "</div>"; //<-- Close body div

            ret += "</li>"; //<-- Close comment_box div
            return ret;
        }
    }
}