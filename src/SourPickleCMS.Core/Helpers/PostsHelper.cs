﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Raven.Client;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Services.DataAccess;

namespace SourPickleCMS.Core.Helpers
{
    public static class PostsHelper
    {
        public static HtmlString PostContent(this HtmlHelper helper, string PostID = "")
        {
            HtmlString content;
            Post p = null;

            using (UnitOfWork unitOfWork = new UnitOfWork((IDocumentSession)HttpContext.Current.Items["CurrentDocumentSession"]))
            {
                if (unitOfWork.Session.Post == null) unitOfWork.Session.Post = new List<Post>();

                if (!String.IsNullOrWhiteSpace(PostID))
                {
                    p = unitOfWork.PostRepository.GetByID(PostID);
                }
                else if (String.IsNullOrWhiteSpace(PostID) && unitOfWork.Session.Post.Count() > 0)
                {
                    if (unitOfWork.Session.CurrentPostPos != null && unitOfWork.Session.CurrentPostPos != 0)
                    {
                        if (unitOfWork.Session.Post.Count() >= unitOfWork.Session.CurrentPostPos)
                        {
                            p = unitOfWork.Session.Post.ElementAt((int)unitOfWork.Session.CurrentPostPos);
                        }
                    }
                    else if (unitOfWork.Session.Post.Count() > 0)
                    {
                        p = unitOfWork.Session.Post.First();
                    }
                    unitOfWork.Session.CurrentPostPos++;
                }
                if (p == null) return null;

                if (p.ContentType.Slug == "media")
                {
                    content = MediaHelpers.GetImage(null, p.Id);
                }
                else
                {
                    content = new HtmlString(p.Body);
                }
            }

            return content;
        }

        public static Post GetPost(this HtmlHelper helper, string Id = null)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork((IDocumentSession)HttpContext.Current.Items["CurrentDocumentSession"]))
            {
                Post post = new Post();

                if (unitOfWork.Session.Post != null && unitOfWork.Session.Post.Count() > 0 && String.IsNullOrWhiteSpace(Id))
                {
                    post = unitOfWork.Session.Post.First();
                }
                else if (!String.IsNullOrWhiteSpace(Id))
                {
                    post = unitOfWork.PostRepository.GetByID(Id);
                }

                return post;
            }
        }

        public static IList<Post> GetPosts(this HtmlHelper helper, string content_type, string max_count)
        {
            return GetPosts(helper, content_type, Convert.ToInt32(max_count));
        }

        public static IList<Post> GetPosts(this HtmlHelper helper, string content_type, int max_count = -1)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork((IDocumentSession)HttpContext.Current.Items["CurrentDocumentSession"]))
            {
                if (max_count == -1)
                {
                    return unitOfWork.PostRepository.GetAllByContentTypeSlug(content_type).OrderBy(o => o.Order).ToList();
                }
                else
                {
                    return unitOfWork.PostRepository.GetAllByContentTypeSlug(content_type).Take(max_count).OrderBy(o => o.Order).ToList();
                }
            }
        }

        public static IList<Category> GetCategories(this HtmlHelper helper, string content_type)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork((IDocumentSession)HttpContext.Current.Items["CurrentDocumentSession"]))
            {
                return unitOfWork.CategoryRepository.GetByContentType(content_type);
            }
        }
    }
}