﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SourPickleCMS.Core.Services.DataAccess;
using SourPickleCMS.Core.Models;
using Raven.Client;

namespace SourPickleCMS.Core.Helpers
{
    public static class MediaHelpers
    {
        public static HtmlString GetImageThumbnail(this HtmlHelper helper, string PostID, object htmlAttributes = null)
        {
            string res = CreateImageString(PostID, htmlAttributes, "tb");
            using (UnitOfWork unitOfWork = new UnitOfWork(null))
            {
                res += " width='" + unitOfWork.Session.Settings["ImageThumbnailWidth"] + "'";
                res += " height='" + unitOfWork.Session.Settings["ImageThumbnailHeight"] + "'";
            }
            res += " />";

            return new HtmlString(res);
        }

        public static HtmlString GetImage(this HtmlHelper helper, string PostID, object htmlAttributes = null, string size = "med")
        {
            string res = "";

            res = CreateImageString(PostID, htmlAttributes, size);
            res += " />";

            return new HtmlString(res);
        }

        public static string CreateImageString(string PostID, object htmlAttributes = null, string size = "med")
        {
            Dictionary<string, string> htmlAttrs = new Dictionary<string,string>();
            if (htmlAttributes != null) htmlAttrs = htmlAttributes.ToDictionary();

            string res = "";

            using (UnitOfWork unitOfWork = new UnitOfWork((IDocumentSession)HttpContext.Current.Items["CurrentDocumentSession"]))
            {
                Post image = unitOfWork.PostRepository.GetByID(PostID);

                string ext = image.Slug.Substring(image.Slug.Length - 4);
                string fname = image.Slug.Replace(ext, "");
                if (size != "" && size != "full")
                {
                    size = "-" + size;
                }
                else
                {
                    size = "";
                }

                res = "<img src='/Uploads/";
                res += image.CreateDate.Year.ToString() + "/";
                res += image.CreateDate.Month.ToString() + "/";
                res += fname + size + ext + "'";
                res += " alt='" + image.AltText + "'";
                res += " title='" + image.AltText + "'";

                foreach (KeyValuePair<String, String> entry in htmlAttrs)
                {
                    string key = entry.Key.Replace("@", "");
                    res += " " + key + "='" + entry.Value + "'";
                }
            }

            return res;
        }
    }
}
