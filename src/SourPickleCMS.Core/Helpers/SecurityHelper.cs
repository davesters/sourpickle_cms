﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel;
using SourPickleCMS.Core.Services.DataAccess;
using Raven.Client;

namespace SourPickleCMS.Core.Models
{
    public static class SecurityHelper
    {
        public static string GetHashedString(string str)
        {
            string rethash = "";
            try
            {
                SHA1 hash = SHA1.Create();
                ASCIIEncoding encoder = new ASCIIEncoding();
                byte[] combined = encoder.GetBytes(str);
                hash.ComputeHash(combined);
                rethash = Convert.ToBase64String(hash.Hash);
            }
            catch
            {
                return "";
            }
            return rethash;
        }

        public static string GetSecureCookieString(string username, string salt, DateTime expires)
        {
            string cookie = username + "|" + expires.ToString() + "|";
            string tempHash = cookie + GetHashedString(cookie + salt);

            return tempHash;
        }

        public static bool CheckIfCookieIsSecure(string receivedCookie, string salt)
        {
            IList<string> data = receivedCookie.Split('|').SkipWhile(d => d.Trim() == "").ToList();
            string cookie = data[0] + "|" + data[1] + "|";
            string newHash = GetHashedString(cookie + salt);

            if (newHash == data[2]) return true; else return false;
        }

        public static bool CheckEmailString(string email)
        {
            Regex reg = new Regex("[^A-Za-z0-9@_.-]");
            return reg.IsMatch(email);
        }

        public static string GetRandomString([DefaultValue(20)]int len)
        {
            string[] randArray = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "e", "i", "o", "u", "r", "s", "t", "n", "x", "y", "z", "A", "E", "I", "O", "U", "R", "S", "T", "N", "X", "Y", "Z" };
            Random saltRnd = new Random();

            string passSalt = "";
            for (int x = 0; x < len; x++)
            {
                passSalt += randArray[saltRnd.Next(randArray.Count())];
            }

            return passSalt;
        }

        public static void ProcessAutoLogin(HttpRequestBase Request)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork((IDocumentSession)HttpContext.Current.Items["CurrentDocumentSession"]))
            {
                try
                {
                    if (Request.Browser.Cookies == true && unitOfWork.Session.LoggedIn == false && Request.Cookies["SOURPICKLELOGIN"]["AUTOLOGIN"] != null)
                    {
                        IList<string> data = Request.Cookies["SOURPICKLELOGIN"]["AUTOLOGIN"].Split('|').SkipWhile(d => d.Trim() == "").ToList();
                        Account account = unitOfWork.AccountRepository.GetByUsername(data[0]);

                        if (account != null)
                        {
                            if (SecurityHelper.CheckIfCookieIsSecure(Request.Cookies["SOURPICKLELOGIN"]["AUTOLOGIN"], account.PasswordSalt) == false)
                            {
                                unitOfWork.Session.CurrentUser = new Account { Role = 0 };
                                return;
                            }

                            Request.Cookies["SOURPICKLELOGIN"].Expires = DateTime.Now.AddDays(30);
                            account.LastLoginDate = DateTime.Now;
                            unitOfWork.Session.CurrentUser = account;
                            unitOfWork.Session.LoggedIn = true;
                        }
                        else
                        {
                            unitOfWork.Session.CurrentUser = new Account { Role = 0 };
                        }
                    }
                }
                catch
                {
                    unitOfWork.Session.CurrentUser = new Account { Role = 0 };
                    return;
                }
            }
        }
    }
}