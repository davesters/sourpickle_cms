﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SourPickleCMS.Core.Attributes
{
    public class BypassAntiForgeryTokenAttribute : ActionFilterAttribute
    {
    }
}