﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Services.DataAccess;
using SourPickleCMS.Core.Controllers;

namespace SourPickleCMS.Core.Attributes
{
    public class RoleAuthorizeAttribute : AuthorizeAttribute
    {
        public new int Roles;

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null) throw new ArgumentNullException("httpContext");
            SecurityHelper.ProcessAutoLogin(httpContext.Request);

            using (UnitOfWork unitOfWork = new UnitOfWork(null))
            {
                if (unitOfWork.Session.LoggedIn == false) return false;

                int role = unitOfWork.Session.CurrentUser.Role;

                if (Roles != 0 && role < Roles)
                {
                    return false;
                }

                return true;
            }
        }
    }

    public static class AuthRoles
    {
        public const int Guest = 0;
        public const int User = 2;
        public const int Writer = 4;
        public const int Publisher = 6;
        public const int Moderator = 8;
        public const int Admin = 10;
    }
}