﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SourPickleCMS.Core.Models;
using Raven.Client;
using SourPickleCMS.Core.Services.DataAccess;
using System.Linq.Expressions;
using SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes;

namespace SourPickleCMS.Core.Services
{
    public class BlockService : IBlockService
    {
        private readonly IGenericRepository<Block> _blockRepository;

        public BlockService(IDocumentSession _session)
        {
            _blockRepository = new GenericRepository<Block>(_session);
        }

        public IList<Block> Get(Expression<Func<Block, bool>> filter = null, Func<IQueryable<Block>, IOrderedQueryable<Block>> orderBy = null)
        {
            IList<Block> query = _blockRepository.Get(filter, orderBy);
            return query;
        }

        public Block GetByID(string id)
        {
            try
            {
                return _blockRepository.GetByID(id);
            }
            catch
            {
                return null;
            }
        }

        public IList<Block> GetBlocksByTheme(string theme)
        {
            return _blockRepository.session.Query<Block, Blocks_ByThemeOrRegion>().Where(b => b.Theme == theme).ToList();
        }

        public IList<Block> GetBlocksByRegion(string region, string theme)
        {
            return _blockRepository.session.Query<Block, Blocks_ByThemeOrRegion>().Where(b => b.Theme == theme && b.Region == region).ToList();
        }

        public Block Save(Block model)
        {
            _blockRepository.Add(model);
            return model;
        }

        public void Update(Block model)
        {
            _blockRepository.Update(model);
        }

        public void Delete(Block model)
        {
            _blockRepository.Delete(model);
        }

        public void Delete(string id)
        {
            _blockRepository.Delete(id);
        }
    }
}
