﻿using SourPickleCMS.Core.Models;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
using System.Linq;

namespace SourPickleCMS.Core.Services
{
    public interface ISettingService
    {
        IList<Setting> Get(Expression<Func<Setting, bool>> filter = null, Func<IQueryable<Setting>, IOrderedQueryable<Setting>> orderBy = null);
        Dictionary<string, string> GetSettingsDictionary();
        IList<Setting> GetThemeSettings(string theme);
        Setting GetSetting(string name, string theme = "");
        void UpdateSetting(string name, string value, bool save = true);
        void UpdateSetting(IList<Setting> model, bool save = true);
        bool AddThemeSettings(string theme, string folder);
        void AddThemeSetting(string name, string value, string label, string theme, bool save = true);
        void UpdateThemeSetting(string name, string value, string theme, bool save = true);
    }
}