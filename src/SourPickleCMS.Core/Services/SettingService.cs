﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Raven.Client;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Services.DataAccess;
using SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes;
using System.IO;

namespace SourPickleCMS.Core.Services
{
    public class SettingService : ISettingService
    {
        private readonly IGenericRepository<Setting> _settingRepository;

        public SettingService(IDocumentSession _session)
        {
            _settingRepository = new GenericRepository<Setting>(_session);
        }

        public IList<Setting> Get(Expression<Func<Setting, bool>> filter = null, Func<IQueryable<Setting>, IOrderedQueryable<Setting>> orderBy = null)
        {
            return _settingRepository.Get(filter, orderBy);
        }

        public Dictionary<string, string> GetSettingsDictionary()
        {
            IList<Setting> settings = _settingRepository.Get();

            var hash = new Dictionary<string, string>(settings.Count());

            foreach (Setting s in settings.Where(s => s.IsCustomSetting == false))
            {
                hash.Add(s.Name, s.Value);
            }

            IList<Setting> themeSettings = GetThemeSettings(hash["Theme"]);
            foreach (Setting ts in themeSettings)
            {
                hash.Add(ts.Name, ts.Value);
            }

            return hash;
        }

        public IList<Setting> GetThemeSettings(string theme)
        {
            return _settingRepository.session.Query<Setting, Settings_ByTheme>().Where(s => s.Theme == theme).ToList();
        }

        public Setting GetSetting(string name, string theme = "")
        {
            if (String.IsNullOrWhiteSpace(theme))
            {
                return _settingRepository.session.Query<Setting, Settings_ByName>().FirstOrDefault(s => s.Name == name);
            }
            else
            {
                return _settingRepository.session.Query<Setting, Settings_ByName>().FirstOrDefault(s => s.Name == name && s.Theme == theme);
            }
        }

        public void UpdateSetting(string name, string value)
        {
            Setting setting = GetSetting(name);
            if (setting != null)
            {
                setting.Value = value;
                _settingRepository.Update(setting);
            }
        }

        public void UpdateSetting(IList<Setting> model)
        {
            foreach (Setting s in model)
            {
                _settingRepository.Update(s);
            }
        }

        public bool AddThemeSettings(string theme, string folder)
        {
            if (File.Exists(folder + "/Settings.txt"))
            {
                StreamReader sr = new StreamReader(folder + "/Settings.txt");
                do
                {
                    string line = sr.ReadLine();
                    if (String.IsNullOrWhiteSpace(line) == true || line.Substring(0, 1) == "*") continue;
                    string[] vals = line.Split(',');
                    string name = "", value = "", label = "";

                    if (vals.Count() > 0)
                    {
                        name = vals[0].Substring(vals[0].IndexOf(':') + 1);

                        if (vals.Count() > 1)
                        {
                            label = vals[1].Trim();
                        }
                        if (vals.Count() > 2)
                        {
                            value = vals[2].Trim();
                        }
                        AddThemeSetting(name.Trim(), value, label, theme);
                    }
                } while (!sr.EndOfStream);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void AddThemeSetting(string name, string value, string label, string theme)
        {
            Setting setting = GetSetting(name, theme);
            if (setting == null)
            {
                setting = new Setting
                {
                    Id = null,
                    Name = name,
                    Value = value,
                    Theme = theme,
                    IsCustomSetting = true,
                    Label = label
                };
                _settingRepository.Add(setting);
            }
        }

        public void UpdateThemeSetting(string name, string value, string theme)
        {
            Setting setting = GetSetting(name, theme);
            if (setting != null)
            {
                setting.Value = value;
                _settingRepository.Update(setting);
            }
        }
    }
}