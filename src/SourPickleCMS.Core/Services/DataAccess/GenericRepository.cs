﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Raven.Client;

namespace SourPickleCMS.Core.Services.DataAccess
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        public IDocumentSession session { get; set; }

        public GenericRepository(IDocumentSession _session)
        {
            session = _session;
        }

        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate = null)
        {
            return session.Query<TEntity>().FirstOrDefault(predicate);
        }
        public virtual TEntity FirstOrDefault<T>(Expression<Func<TEntity, bool>> predicate = null)
        {
            return session.Query<TEntity>().FirstOrDefault(predicate);
        }

        public virtual IList<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            IQueryable<TEntity> query = session.Query<TEntity>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual IList<TEntity> All()
        {
            return session.Query<TEntity>().ToList();
        }

        public virtual TEntity GetByID(string id)
        {
            return session.Load<TEntity>(id);
        }

        public virtual void Delete(string idToDelete)
        {
            session.Delete(idToDelete);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            session.Delete<TEntity>(entityToDelete);
        }

        public virtual void Add(TEntity entityToAdd)
        {
            session.Store(entityToAdd);
        }

        public virtual void Update(TEntity entityToAdd)
        {
            session.Store(entityToAdd);
        }

        public virtual void Save()
        {
            session.SaveChanges();
        }
    }
}