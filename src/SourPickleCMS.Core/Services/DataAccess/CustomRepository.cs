﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client;
using System.Linq.Expressions;

namespace SourPickleCMS.Core.Services.DataAccess
{
    public class CustomRepository : ICustomRepository
    {
        public IDocumentSession session { get; set; }

        public CustomRepository(IDocumentSession _session)
        {
            session = _session;
        }

        public virtual void Delete(string idToDelete)
        {
            session.Delete(idToDelete);
        }

        public virtual void Delete(Object entityToDelete)
        {
            session.Delete<Object>(entityToDelete);
        }

        public virtual void Add(Object entityToAdd)
        {
            session.Store(entityToAdd);
        }

        public virtual void Update(Object entityToAdd)
        {
            session.Store(entityToAdd);
        }

        public virtual void Save()
        {
            session.SaveChanges();
        }
    }
}