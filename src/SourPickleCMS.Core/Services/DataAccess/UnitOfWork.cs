﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SourPickleCMS.Core.Models;
using Raven.Client.Document;
using Raven.Client;

namespace SourPickleCMS.Core.Services.DataAccess
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        public IDocumentSession _documentSession;
        private bool _disposed = false;
        private Session session;

        private IAccountService _accountRepository;
        private ISettingService _settingRepository;
        private ILogService _logRepository;
        private IPostService _postRepository;
        private IContentTypeService _contentTypeRepository;
        private ICategoryService _categoryRepository;
        private IBlockService _blockRepository;
        private IVoteService _voteRepository;
        private ICommentService _commentRepository;
        
        public UnitOfWork(IDocumentSession session)
        {
            _documentSession = session;
        }

        public Session Session
        {
            get
            {
                if (this.session == null)
                {
                    this.session = new Session();
                }
                return session;
            }
        }

        public IBlockService BlockRepository
        {
            get
            {
                if (_blockRepository == null)
                {
                    _blockRepository = new BlockService(_documentSession);
                }
                return _blockRepository;
            }
        }

        public IVoteService VoteRepository
        {
            get
            {
                if (_voteRepository == null)
                {
                    _voteRepository = new VoteService(_documentSession);
                }
                return _voteRepository;
            }
        }

        public ICommentService CommentRepository
        {
            get
            {
                if (_commentRepository == null)
                {
                    _commentRepository = new CommentService(_documentSession);
                }
                return _commentRepository;
            }
        }

        public IContentTypeService ContentTypeRepository
        {
            get
            {
                if (_contentTypeRepository == null)
                {
                    _contentTypeRepository = new ContentTypeService(_documentSession);
                }
                return _contentTypeRepository;
            }
        }

        public ICategoryService CategoryRepository
        {
            get
            {
                if (_categoryRepository == null)
                {
                    _categoryRepository = new CategoryService(_documentSession);
                }
                return _categoryRepository;
            }
        }

        public ILogService LogRepository
        {
            get
            {
                if (_logRepository == null)
                {
                    _logRepository = new LogService(_documentSession);
                }
                return _logRepository;
            }
        }

        public ISettingService SettingRepository
        {
            get
            {
                if (_settingRepository == null)
                {
                    _settingRepository = new SettingService(_documentSession);
                }
                return _settingRepository;
            }
        }

        public IPostService PostRepository
        {
            get
            {
                if (_postRepository == null)
                {
                    _postRepository = new PostService(_documentSession);
                }
                return _postRepository;
            }
        }

        public IAccountService AccountRepository
        {
            get
            {
                if (_accountRepository == null)
                {
                    _accountRepository = new AccountService(_documentSession);
                }
                return _accountRepository;
            }
        }

        public void Save()
        {
            _documentSession.SaveChanges();
        }

        public IDocumentSession DocumentSession
        {
            set
            {
                this._documentSession = value;
            }
        }

        protected virtual void DisposeSession(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing && _documentSession != null)
                {
                    _documentSession.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            DisposeSession(true);
            GC.SuppressFinalize(this);
        }
    }
}