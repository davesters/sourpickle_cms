﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client;
using System.Linq.Expressions;

namespace SourPickleCMS.Core.Services.DataAccess
{
    public interface ICustomRepository
    {
        IDocumentSession session { get; set; }

        void Delete(string idToDelete);
        void Delete(Object entityToDelete);
        void Add(Object entityToAdd);
        void Update(Object entityToAdd);
        void Save();
    }
}
