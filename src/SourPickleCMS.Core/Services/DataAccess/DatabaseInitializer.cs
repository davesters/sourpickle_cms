﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client.Document;
using SourPickleCMS.Core.Models;
using System.Collections.ObjectModel;

namespace SourPickleCMS.Core.Services.DataAccess
{
    public static class DatabaseInitializer
    {
        public static void Initialize(DocumentSession session)
        {
            // ************* Site Settings ****************
            session.Store(new Setting { Id = "settings-1", IsCustomSetting = false, Name = "SiteTitle", Value = "Sour Pickle CMS" });
            session.Store(new Setting { Id = "settings-2", IsCustomSetting = false, Name = "SiteURL", Value = "http://www.sourpickle-cms.com" });
            session.Store(new Setting { Id = "settings-3", IsCustomSetting = false, Name = "MetaDescription", Value = "An extremely flexible and customizable 'cms' built with MVC 4 for .net developers" });
            session.Store(new Setting { Id = "settings-4", IsCustomSetting = false, Name = "TagLine", Value = "An extremely flexible and customizable 'cms' built with MVC 4 for .net developers" });
            session.Store(new Setting { Id = "settings-5", IsCustomSetting = false, Name = "MetaKeywords", Value = ".net, cms, mvc 4, asp, asp.net, entity framework, code first, open source, free, content management system, lightweight, simple" });
            session.Store(new Setting { Id = "settings-6", IsCustomSetting = false, Name = "AdminEmail", Value = "" });
            session.Store(new Setting { Id = "settings-7", IsCustomSetting = false, Name = "RegistrationOpen", Value = "true" });
            session.Store(new Setting { Id = "settings-8", IsCustomSetting = false, Name = "DefaultUserRoleID", Value = "2" });
            session.Store(new Setting { Id = "settings-9", IsCustomSetting = false, Name = "Theme", Value = "SourPickle" });
            session.Store(new Setting { Id = "settings-10", IsCustomSetting = false, Name = "IncludeJQuery", Value = "true" });
            session.Store(new Setting { Id = "settings-11", IsCustomSetting = false, Name = "StyleFramework", Value = "960_12" });
            session.Store(new Setting { Id = "settings-12", IsCustomSetting = false, Name = "SiteMode", Value = "live" });
            session.Store(new Setting { Id = "settings-13", IsCustomSetting = false, Name = "RequireAgreeToTerms", Value = "true" });
            session.Store(new Setting { Id = "settings-14", IsCustomSetting = false, Name = "LoggingEnabled", Value = "true" });
            session.Store(new Setting { Id = "settings-15", IsCustomSetting = false, Name = "AllowRememberLogin", Value = "true" });
            session.Store(new Setting { Id = "settings-16", IsCustomSetting = false, Name = "ImageThumbnailHeight", Value = "120" });
            session.Store(new Setting { Id = "settings-17", IsCustomSetting = false, Name = "ImageThumbnailWidth", Value = "120" });
            session.Store(new Setting { Id = "settings-18", IsCustomSetting = false, Name = "ImageMediumMaxSize", Value = "500" });
            session.Store(new Setting { Id = "settings-19", IsCustomSetting = false, Name = "ImageLargeMaxSize", Value = "800" });
            session.Store(new Setting { Id = "settings-20", IsCustomSetting = false, Name = "MaxFileUploadSize", Value = "2" });
            session.Store(new Setting { Id = "settings-21", IsCustomSetting = false, Name = "CustomStyles", Value = "" });
            session.Store(new Setting { Id = "settings-22", IsCustomSetting = false, Name = "CustomScripts", Value = "" });
            session.Store(new Setting { Id = "settings-23", IsCustomSetting = false, Name = "AjaxInComments", Value = "false" });
            session.Store(new Setting { Id = "settings-24", IsCustomSetting = false, Name = "EmailHost", Value = "" });
            session.Store(new Setting { Id = "settings-25", IsCustomSetting = false, Name = "EmailAuthentication", Value = "false" });
            session.Store(new Setting { Id = "settings-26", IsCustomSetting = false, Name = "EmailUseSSL", Value = "false" });
            session.Store(new Setting { Id = "settings-27", IsCustomSetting = false, Name = "EmailUsername", Value = "" });
            session.Store(new Setting { Id = "settings-28", IsCustomSetting = false, Name = "EmailPassword", Value = "" });
            session.Store(new Setting { Id = "settings-29", IsCustomSetting = false, Name = "EmailFrom", Value = "" });
            session.Store(new Setting { Id = "settings-30", IsCustomSetting = false, Name = "EmailFromAddress", Value = "" });
            session.Store(new Setting { Id = "settings-31", IsCustomSetting = false, Name = "EmailPort", Value = "25" });
            session.SaveChanges();

            
            
            // ************* ACCOUNTS ****************
            Account account = new Account
            {
                Id = "accounts-1",
                Name = "Admin",
                Email = "admin@yourwebsite.com",
                Username = "Admin",
                EmailVerified = true,
                AgreeToTerms = true,
                IsLocked = false,
                IsSuspended = false,
                Password = "admin",
                JoinDate = DateTime.Now,
                LastLoginDate = DateTime.Now,
                RegistrationComplete = true,
                Role = 10,
                Karma = 0
            };
            account.CreateNewPassword();
            session.Store(account);
            session.SaveChanges();



            // ************* Content TypeS ****************
            session.Store(new ContentType
            {
                Id = "ContentTypes-1",
                Name = "Post",
                Slug = "blog",
                PluralName = "Posts",
                Commentable = true,
                DefaultTemplate = "Blog",
                UsesEditor = true,
                IsCustom = false,
            });
            session.Store(new ContentType
            {
                Id = "ContentTypes-2",
                Name = "Page",
                Slug = "page",
                PluralName = "Pages",
                Commentable = false,
                DefaultTemplate = "Default",
                UsesEditor = true,
                IsCustom = false
            });
            session.Store(new ContentType
            {
                Id = "ContentTypes-3",
                Name = "Menu Item",
                Slug = "menu-item",
                PluralName = "Menu Items",
                Commentable = false,
                UsesEditor = false,
                IsCustom = false
            });
            session.Store(new ContentType
            {
                Id = "ContentTypes-4",
                Name = "Media",
                Slug = "media",
                PluralName = "Media",
                Commentable = true,
                DefaultTemplate = "Default",
                UsesEditor = false,
                IsCustom = false
            });
            session.SaveChanges();



            // ************* BLOCKS ****************
            session.Store(new Block
            {
                Id = "blocks-1",
                Title = "Most Recent Posts",
                Name = "MostRecentPosts",
                Region = "Sidebar",
                Theme = "SourPickle",
                Description = "Display a list of the most recent posts for any content type.",
                Directory = "D:\\Websites\\SourPickleCMS\\src\\SourPickleCMS.Core.Admin\\Blocks",
                Order = "0",
                Fields = new Dictionary<string, string>
                {
                    { "Title", "Most Recent Posts" },
                    { "ContentType", "blog" },
                    { "MaxPosts", "5" }
                }
            });
            session.Store(new Block
            {
                Id = "blocks-2",
                Title = "Category List",
                Name = "CategoryList",
                Region = "Sidebar",
                Theme = "SourPickle",
                Description = "Display a list of the all categories for a certain content type.",
                Directory = "D:\\Websites\\SourPickleCMS\\src\\SourPickleCMS.Core.Admin\\Blocks",
                Order = "1",
                Fields = new Dictionary<string, string>
                {
                    { "Title", "Categories" },
                    { "ContentType", "blog" }
                }
            });



            // ************* CATEGORIES ****************
            session.Store(new Category
            {
                Id = "categories-1",
                Name = "Uncategorized",
                Slug = "uncategorized",
                ContentType = "blog",
                DefaultTemplate = "Blog"
            });
            session.Store(new Category
            {
                Id = "categories-2",
                Name = "Primary Navigation",
                Slug = "primary-navigation",
                ContentType = "menu-item"
            });
            session.Store(new Category
            {
                Id = "categories-3",
                Name = "Uncategorized",
                Slug = "uncategorized",
                ContentType = "media",
                DefaultTemplate = "Default"
            });
            session.SaveChanges();



            // ************* Posts ****************
            session.Store(new Post
            {
                Id = "posts-1",
                ContentType = new ContentType
                {
                    Name = "Page",
                    Slug = "page",
                    PluralName = "Pages",
                    Commentable = true,
                    DefaultTemplate = "Blog"
                },
                Body = "This is a sample home page",
                CreateDate = DateTime.Parse("2011-09-15"),
                LastUpdatedDate = DateTime.Parse("2011-09-15"),
                Slug = "home",
                Title = "Welcome to Sour Pickle CMS!",
                Username = "Admin",
                Status = "published",
                Role = 0,
                TemplateName = "Home",
                ParentID = ""
            });
            session.Store(new Post
            {
                Id = "posts-2",
                ContentType = new ContentType
                {
                    Name = "Page",
                    Slug = "page",
                    PluralName = "Pages",
                    Commentable = true,
                    DefaultTemplate = "Blog"
                },
                Body = "This is a sample about page",
                CreateDate = DateTime.Parse("2011-09-15"),
                LastUpdatedDate = DateTime.Parse("2011-09-15"),
                Slug = "about",
                Title = "About Sour Pickle CMS",
                Username = "Admin",
                Status = "published",
                Role = 0,
                TemplateName = "Default",
                ParentID = ""
            });
            session.Store(new Post
            {
                Id = "posts-3",
                ContentType = new ContentType
                {
                    Name = "Post",
                    Slug = "blog",
                    PluralName = "Posts",
                    Commentable = true,
                    DefaultTemplate = "Blog"
                },
                Category = new Category
                {
                    Name = "Uncategorized",
                    Slug = "uncategorized",
                    ContentType = "post",
                    DefaultTemplate = "Blog"
                },
                Body = "This is a sample blog post.  You can edit this post, or delete it and create your own.",
                CreateDate = DateTime.Parse("2011-09-15"),
                LastUpdatedDate = DateTime.Parse("2011-09-15"),
                Slug = "sample-blog-post",
                Title = "Sample Blog Post",
                Username = "Admin",
                Status = "published",
                Role = 0,
                TemplateName = "Default",
                ParentID = ""
            });
            session.Store(new Post
            {
                Id = "posts-4",
                ContentType = new ContentType
                {
                    Name = "Menu Item",
                    Slug = "menu-item",
                    PluralName = "Menu Items",
                    Commentable = true
                },
                Category = new Category
                {
                    Name = "Primary Navigation",
                    Slug = "primary-navigation",
                    ContentType = "menu-item"
                },
                Fields = new List<Field> {
                    new Field
                    {
                        IsCustomField = false,
                        Name = "MenuPostID",
                        Value = "posts-1",
                    },
                    new Field
                    {
                        IsCustomField = false,
                        Name = "Link",
                        Value = "/",
                    }
                },
                CreateDate = DateTime.Parse("2011-09-15"),
                LastUpdatedDate = DateTime.Parse("2011-09-15"),
                Slug = "home",
                Title = "Home",
                Username = "Admin",
                Order = "1",
                Status = "published"
            });
            session.Store(new Post
            {
                Id = "posts-5",
                ContentType = new ContentType
                {
                    Name = "Menu Item",
                    Slug = "menu-item",
                    PluralName = "Menu Items",
                    Commentable = true
                },
                Category = new Category
                {
                    Name = "Primary Navigation",
                    Slug = "primary-navigation",
                    ContentType = "menu-item"
                },
                Fields = new List<Field> {
                    new Field
                    {
                        IsCustomField = false,
                        Name = "MenuPostID",
                        Value = "posts-2",
                    },
                    new Field
                    {
                        IsCustomField = false,
                        Name = "Link",
                        Value = "/about",
                    }
                },
                CreateDate = DateTime.Parse("2011-09-15"),
                LastUpdatedDate = DateTime.Parse("2011-09-15"),
                Slug = "about",
                Title = "About",
                Username = "Admin",
                Order = "2",
                Status = "published"
            });
            session.Store(new Post
            {
                Id = "posts-6",
                ContentType = new ContentType
                {
                    Name = "Menu Item",
                    Slug = "menu-item",
                    PluralName = "Menu Items",
                    Commentable = true
                },
                Category = new Category
                {
                    Name = "Primary Navigation",
                    Slug = "primary-navigation",
                    ContentType = "menu-item"
                },
                Fields = new List<Field> {
                    new Field
                    {
                        IsCustomField = false,
                        Name = "MenuPostID",
                        Value = "",
                    },
                    new Field
                    {
                        IsCustomField = false,
                        Name = "Link",
                        Value = "/blog",
                    }
                },
                CreateDate = DateTime.Parse("2011-09-15"),
                LastUpdatedDate = DateTime.Parse("2011-09-15"),
                Slug = "blog",
                Title = "Blog",
                Username = "Admin",
                Order = "3",
                Status = "published"
            });
            session.SaveChanges();
        }
    }
}