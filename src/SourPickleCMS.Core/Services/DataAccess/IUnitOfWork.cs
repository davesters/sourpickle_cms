﻿using SourPickleCMS.Core.Models;
using Raven.Client;

namespace SourPickleCMS.Core.Services.DataAccess
{
    public interface IUnitOfWork
    {
        Session Session { get; }
        IContentTypeService ContentTypeRepository { get; }
        ICategoryService CategoryRepository { get; }
        ILogService LogRepository { get; }
        ISettingService SettingRepository { get; }
        IPostService PostRepository { get; }
        IAccountService AccountRepository { get; }
        IBlockService BlockRepository { get; }
        IVoteService VoteRepository { get; }
        ICommentService CommentRepository { get; }
        IDocumentSession DocumentSession { set; }
        void Save();
        void Dispose();
    }
}