﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Raven.Client;

namespace SourPickleCMS.Core.Services.DataAccess
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IDocumentSession session { get; set; }

        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate = null);
        TEntity FirstOrDefault<T>(Expression<Func<TEntity, bool>> predicate = null);

        IList<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null);
        IList<TEntity> All();
        TEntity GetByID(string id);
        void Delete(string idToDelete);
        void Delete(TEntity entityToDelete);
        void Add(TEntity entityToAdd);
        void Update(TEntity entityToAdd);
        void Save();
    }
}