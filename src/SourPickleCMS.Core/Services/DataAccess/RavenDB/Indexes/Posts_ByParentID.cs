﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client.Indexes;
using SourPickleCMS.Core.Models;

namespace SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes
{
    public class Posts_ByParentID : AbstractIndexCreationTask<Post>
    {
        public Posts_ByParentID()
        {
            Map = posts => from post in posts
                           select new { ParentID = post.ParentID, Status = post.Status };
        }
    }
}
