﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client.Indexes;
using SourPickleCMS.Core.Models;

namespace SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes
{
    public class ContentTypes_ByCustom : AbstractIndexCreationTask<ContentType>
    {
        public ContentTypes_ByCustom()
        {
            Map = ContentTypes => from ContentType in ContentTypes
                               select new { IsCustom = ContentType.IsCustom };
        }
    }
}