﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client.Indexes;
using SourPickleCMS.Core.Models;

namespace SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes
{
    public class Settings_ByTheme : AbstractIndexCreationTask<Setting>
    {
        public Settings_ByTheme()
        {
            Map = settings => from setting in settings
                              select new { Theme = setting.Theme, IsCustomSettomg = setting.IsCustomSetting };
        }
    }
}
