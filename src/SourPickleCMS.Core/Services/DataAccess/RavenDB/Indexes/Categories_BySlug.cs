﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client.Indexes;
using SourPickleCMS.Core.Models;

namespace SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes
{
    public class Categories_BySlug : AbstractIndexCreationTask<Category>
    {
        public Categories_BySlug()
        {
            Map = categories => from category in categories select new { Slug = category.Slug, ContentType = category.ContentType };
        }
    }
}
