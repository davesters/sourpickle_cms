﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client.Indexes;
using SourPickleCMS.Core.Models;

namespace SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes
{
    public class Posts_BySlug : AbstractIndexCreationTask<Post>
    {
        public Posts_BySlug()
        {
            Map = posts => from post in posts select new { Slug = post.Slug, Category_Slug = post.Category.Slug, ContentType_Slug = post.ContentType.Slug, Status = post.Status };
        }
    }
}