﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client.Indexes;
using SourPickleCMS.Core.Models;

namespace SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes
{
    public class Blocks_ByThemeOrRegion : AbstractIndexCreationTask<Block>
    {
        public Blocks_ByThemeOrRegion()
        {
            Map = blocks => from block in blocks select new { Theme = block.Theme, Region = block.Region };
        }
    }
}
