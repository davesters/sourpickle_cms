﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client.Indexes;
using SourPickleCMS.Core.Models;

namespace SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes
{
    public class ContentTypes_BySlug : AbstractIndexCreationTask<ContentType>
    {
        public ContentTypes_BySlug()
        {
            Map = ContentTypes => from ContentType in ContentTypes
                               select new { Slug = ContentType.Slug };
        }
    }
}
