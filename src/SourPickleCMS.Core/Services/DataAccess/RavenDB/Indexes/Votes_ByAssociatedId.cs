﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raven.Client.Indexes;
using SourPickleCMS.Core.Models;

namespace SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes
{
    public class Votes_ByAssociatedId : AbstractIndexCreationTask<Vote>
    {
        public Votes_ByAssociatedId()
        {
            Map = votes => from vote in votes
                           select new { AssociatedId = vote.AssociatedId, Username = vote.Username };
        }
    }
}
