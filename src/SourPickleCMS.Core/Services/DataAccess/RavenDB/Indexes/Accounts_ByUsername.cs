﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SourPickleCMS.Core.Models;
using Raven.Client.Indexes;

namespace SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes
{
    public class Accounts_ByUsername : AbstractIndexCreationTask<Account>
    {
        public Accounts_ByUsername()
        {
            Map = accounts => from account in accounts
                              select new { Username = account.Username };
        }
    }
}