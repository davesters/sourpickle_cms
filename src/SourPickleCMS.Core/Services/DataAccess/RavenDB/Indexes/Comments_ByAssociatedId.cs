﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raven.Client.Indexes;
using SourPickleCMS.Core.Models;

namespace SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes
{
    public class Comments_ByAssociatedId : AbstractIndexCreationTask<Comment>
    {
        public Comments_ByAssociatedId()
        {
            Map = comments => from comment in comments
                           select new { AssociatedId = comment.AssociatedId, Username = comment.Username };
        }
    }
}