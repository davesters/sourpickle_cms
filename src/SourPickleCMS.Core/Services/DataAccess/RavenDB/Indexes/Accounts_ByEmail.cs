﻿using System.Linq;
using Raven.Client.Indexes;
using SourPickleCMS.Core.Models;

namespace SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes
{
    public class Accounts_ByEmail : AbstractIndexCreationTask<Account>
    {
        public Accounts_ByEmail()
        {
            Map = accounts => from account in accounts
                              select new { Email = account.Email };
        }
    }
}
