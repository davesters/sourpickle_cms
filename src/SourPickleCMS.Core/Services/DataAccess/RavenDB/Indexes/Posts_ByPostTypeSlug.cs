﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client.Indexes;
using SourPickleCMS.Core.Models;

namespace SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes
{
    public class Posts_ByContentTypeSlug : AbstractIndexCreationTask<Post>
    {
        public Posts_ByContentTypeSlug()
        {
            Map = posts => from post in posts
                           select new { ContentType_Slug = post.ContentType.Slug, Status = post.Status };
        }
    }
}
