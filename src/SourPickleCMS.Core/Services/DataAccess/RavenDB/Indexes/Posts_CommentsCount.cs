﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SourPickleCMS.Core.Models;
using Raven.Client.Indexes;

namespace SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes
{
    public class Posts_CommentsCount : AbstractIndexCreationTask<Post, Posts_CommentsCount.ReduceResult>
    {
        public class ReduceResult
        {
            public int Count { get; set; }
        }

        public Posts_CommentsCount()
        {
            Map = posts => from post in posts
                           select new { Count = post.Comments.Count };

            Reduce = results => from result in results
                                group result by result
                                into g
                                select new { Count = g.Sum(x => x.Count) };
        }
    }
}
