﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client.Indexes;
using SourPickleCMS.Core.Models;

namespace SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes
{
    public class Posts_ByCategorySlug : AbstractIndexCreationTask<Post>
    {
        public Posts_ByCategorySlug()
        {
            Map = posts => from post in posts
                           select new { Category_Slug = post.Category.Slug, ContentType_Slug = post.ContentType.Slug, Status = post.Status };
        }
    }
}