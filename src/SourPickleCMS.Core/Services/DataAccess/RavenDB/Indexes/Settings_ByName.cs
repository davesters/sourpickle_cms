﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client.Indexes;
using SourPickleCMS.Core.Models;

namespace SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes
{
    public class Settings_ByName : AbstractIndexCreationTask<Setting>
    {
        public Settings_ByName()
        {
            Map = settings => from setting in settings
                              select new { Name = setting.Name, Theme = setting.Theme };
        }
    }
}
