﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SourPickleCMS.Core.Models;
using System.Linq.Expressions;

namespace SourPickleCMS.Core.Services
{
    public interface IBlockService
    {
        IList<Block> Get(Expression<Func<Block, bool>> filter = null, Func<IQueryable<Block>, IOrderedQueryable<Block>> orderBy = null);
        Block GetByID(string id);
        IList<Block> GetBlocksByTheme(string theme);
        IList<Block> GetBlocksByRegion(string region, string theme);
        Block Save(Block model);
        void Update(Block model);
        void Delete(Block model);
        void Delete(string id);
    }
}