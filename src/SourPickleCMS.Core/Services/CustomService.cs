﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client;
using SourPickleCMS.Core.Services.DataAccess;
using System.Linq.Expressions;
using SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes;

namespace SourPickleCMS.Core.Services
{
    public class CustomService : ICustomService
    {
        private readonly ICustomRepository _customRepository;

        public CustomService(IDocumentSession _session)
        {
            _customRepository = new CustomRepository(_session);
        }

        public IDocumentSession Session
        {
            get
            {
                return _customRepository.session;
            }
        }

        public Object Save(Object model)
        {
            _customRepository.Add(model);
            return model;
        }

        public void Update(Object model)
        {
            _customRepository.Update(model);
        }

        public void Delete(Object model)
        {
            _customRepository.Delete(model);
        }

        public void Delete(string id)
        {
            _customRepository.Delete(id);
        }
    }
}