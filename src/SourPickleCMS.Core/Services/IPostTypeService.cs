﻿using SourPickleCMS.Core.Models;
using System.Linq;
using System;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace SourPickleCMS.Core.Services
{
    public interface IContentTypeService
    {
        IList<ContentType> Get(Expression<Func<ContentType, bool>> filter = null, Func<IQueryable<ContentType>, IOrderedQueryable<ContentType>> orderBy = null);
        ContentType GetBySlug(string slug);
        IList<ContentType> GetCustomContentTypes();
        ContentType Save(ContentType model);
        void Update(ContentType model);
        void Delete(ContentType model);
    }
}