﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Raven.Client;

namespace SourPickleCMS.Core.Services
{
    public interface ICustomService
    {
        IDocumentSession Session { get; }
        Object Save(Object model);
        void Update(Object model);
        void Delete(Object model);
        void Delete(string id);
    }
}