﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Raven.Client;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Services.DataAccess;
using SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes;
using Raven.Client.Linq;

namespace SourPickleCMS.Core.Services
{
    public class AccountService : IAccountService
    {
        private readonly IGenericRepository<Account> _accountRepository;

        public AccountService(IDocumentSession _session)
        {
            _accountRepository = new GenericRepository<Account>(_session);
        }

        public IList<Account> Get(Expression<Func<Account, bool>> filter = null, Func<IQueryable<Account>, IOrderedQueryable<Account>> orderBy = null)
        {
            IList<Account> query = _accountRepository.Get(filter, orderBy);
            return query;
        }

        public Account GetByID(string id)
        {
            return _accountRepository.FirstOrDefault(p => p.Id == id);
        }

        public Account GetByEmail(string email)
        {
            return _accountRepository.session.Query<Account, Accounts_ByEmail>().FirstOrDefault(a => a.Email == email);
        }

        public Account GetByUsername(string username)
        {
            return _accountRepository.session.Query<Account, Accounts_ByUsername>().FirstOrDefault(a => a.Username == username);
        }

        public Account CreateNewUser(Account model)
        {
            model.CreateNewPassword();
            _accountRepository.Add(model);

            return model;
        }

        public int AccountCount()
        {
            RavenQueryStatistics stats = new RavenQueryStatistics();

            var results = _accountRepository.session.Query<Account>().Statistics(out stats).ToArray();
            return stats.TotalResults;
        }

        public void Update(Account model)
        {
            _accountRepository.Update(model);
        }

        public void Delete(Account model)
        {
            _accountRepository.Delete(model);
        }
    }
}