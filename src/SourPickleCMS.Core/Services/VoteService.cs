﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Raven.Client;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Services.DataAccess;
using SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes;

namespace SourPickleCMS.Core.Services
{
    public class VoteService : IVoteService
    {
        private readonly IGenericRepository<Vote> _voteRepository;

        public VoteService(IDocumentSession _session)
        {
            _voteRepository = new GenericRepository<Vote>(_session);
        }

        public IList<Vote> Get(Expression<Func<Vote, bool>> filter = null, Func<IQueryable<Vote>, IOrderedQueryable<Vote>> orderBy = null)
        {
            IList<Vote> query = _voteRepository.Get(filter, orderBy);
            return query;
        }

        public IList<Vote> GetByAssociatedId(string id, string Username = null)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(Username))
                {
                    return _voteRepository.session.Query<Vote, Votes_ByAssociatedId>().Where(v => v.AssociatedId == id).ToList();
                }
                else
                {
                    return _voteRepository.session.Query<Vote, Votes_ByAssociatedId>().Where(v => v.AssociatedId == id && v.Username == Username).ToList();
                }
            }
            catch
            {
                return null;
            }
        }

        public Vote Save(Vote model)
        {
            _voteRepository.Add(model);
            return model;
        }

        public void Update(Vote model)
        {
            _voteRepository.Update(model);
        }

        public void Delete(string id)
        {
            _voteRepository.Delete(id);
        }
    }
}
