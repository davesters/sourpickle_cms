﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SourPickleCMS.Core.Models;
using System.Linq.Expressions;

namespace SourPickleCMS.Core.Services
{
    public interface IAccountService
    {
        IList<Account> Get(Expression<Func<Account, bool>> filter = null, Func<IQueryable<Account>, IOrderedQueryable<Account>> orderBy = null);
        Account GetByID(string id);
        Account GetByEmail(string id);
        Account GetByUsername(string username);
        Account CreateNewUser(Account model);
        int AccountCount();
        void Update(Account model);
        void Delete(Account model);
    }
}