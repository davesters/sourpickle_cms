﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Models.ViewModels;

namespace SourPickleCMS.Core.Services
{
    public interface IPostService
    {
        IList<Post> Get(Expression<Func<Post, bool>> filter = null, Func<IQueryable<Post>, IOrderedQueryable<Post>> orderBy = null);
        IList<Post> GetPostsByCategorySlug(string category);
        IList<Post> GetByContentTypeSlug(string type);
        IList<Post> GetAllByContentTypeSlug(string type);
        IList<Post> GetPostsByTypeAndCategory(string category, string type);
        Post GetPostBySlugByTypeAndCategory(string slug, string category, string type);
        Post Save(Post model);
        Post GetBySlug(string slug);
        Post GetBySlugAndCategory(string slug, string category);
        Post GetBySlugAndType(string slug, string type);
        Post GetByID(string id);
        IList<Post> GetPostRevisionsByParentID(string id);
        int GetPostCountByType(string type);
        int GetPostCountByCategory(string cat, string type);
        int TotalCommentsCount();
        void SaveComment(Comment model, string postid);
        bool AddCustomField(Field model, string postid);
        bool UpdateCustomField(Field model, string postid);
        bool DeleteCustomField(Field model, string postid);
        void Update(Post model);
        void Delete(Post model);
    }
}