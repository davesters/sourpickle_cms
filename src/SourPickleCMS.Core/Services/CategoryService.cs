﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Raven.Client;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Services.DataAccess;
using SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes;

namespace SourPickleCMS.Core.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IGenericRepository<Category> _categoryRepository;

        public CategoryService(IDocumentSession _session)
        {
            _categoryRepository = new GenericRepository<Category>(_session);
        }

        public IList<Category> Get(Expression<Func<Category, bool>> filter = null, Func<IQueryable<Category>, IOrderedQueryable<Category>> orderBy = null)
        {
            IList<Category> query = _categoryRepository.Get(filter, orderBy);
            return query;
        }

        public Category GetBySlug(string slug, string type = "")
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(type))
                {
                    return _categoryRepository.session.Query<Category, Categories_BySlug>().FirstOrDefault(c => c.Slug == slug && c.ContentType == type);
                }
                else
                {
                    return _categoryRepository.session.Query<Category, Categories_BySlug>().FirstOrDefault(c => c.Slug == slug);
                }
            }
            catch
            {
                return null;
            }
        }

        public IList<Category> GetByContentType(string type)
        {
            return _categoryRepository.session.Query<Category, Categories_BySlug>().Where(c => c.ContentType == type).ToList();
        }

        public Category GetByID(string id)
        {
            try
            {
                return _categoryRepository.FirstOrDefault(p => p.Id == id);
            }
            catch
            {
                return null;
            }
        }

        public Category Save(Category model)
        {
            _categoryRepository.Add(model);
            return model;
        }

        public void Update(Category model)
        {
            _categoryRepository.Update(model);
        }

        public void Delete(Category model)
        {
            _categoryRepository.Delete(model);
        }

        public void Delete(string id)
        {
            _categoryRepository.Delete(id);
        }
    }
}
