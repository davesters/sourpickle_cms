﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Raven.Client;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Services.DataAccess;

namespace SourPickleCMS.Core.Services
{
    public class LogService : ILogService
    {
        private readonly IGenericRepository<Log> _logRepository;

        public LogService(IDocumentSession _session)
        {
            _logRepository = new GenericRepository<Log>(_session);
        }

        public IList<Log> Get(Expression<Func<Log, bool>> filter = null, Func<IQueryable<Log>, IOrderedQueryable<Log>> orderBy = null)
        {
            IList<Log> query = _logRepository.Get(filter, orderBy);
            return query;
        }
    }
}