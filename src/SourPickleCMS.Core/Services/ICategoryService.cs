﻿using SourPickleCMS.Core.Models;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
using System.Linq;

namespace SourPickleCMS.Core.Services
{
    public interface ICategoryService
    {
        IList<Category> Get(Expression<Func<Category, bool>> filter = null, Func<IQueryable<Category>, IOrderedQueryable<Category>> orderBy = null);
        IList<Category> GetByContentType(string type);
        Category GetBySlug(string slug, string type = "");
        Category GetByID(string id);
        Category Save(Category model);
        void Update(Category model);
        void Delete(Category model);
        void Delete(string id);
    }
}