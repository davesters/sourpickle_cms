﻿using SourPickleCMS.Core.Models;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
using System.Linq;

namespace SourPickleCMS.Core.Services
{
    public interface ICommentService
    {
        IList<Comment> Get(Expression<Func<Comment, bool>> filter = null, Func<IQueryable<Comment>, IOrderedQueryable<Comment>> orderBy = null);
        IList<Comment> GetByAssociatedId(string id, string Username = null);
        Comment Save(Comment model);
        void Update(Comment model);
        void Delete(string id);
    }
}