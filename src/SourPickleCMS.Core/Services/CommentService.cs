﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Raven.Client;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Services.DataAccess;
using SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes;

namespace SourPickleCMS.Core.Services
{
    public class CommentService : ICommentService
    {
        private readonly IGenericRepository<Comment> _commentRepository;

        public CommentService(IDocumentSession _session)
        {
            _commentRepository = new GenericRepository<Comment>(_session);
        }

        public IList<Comment> Get(Expression<Func<Comment, bool>> filter = null, Func<IQueryable<Comment>, IOrderedQueryable<Comment>> orderBy = null)
        {
            IList<Comment> query = _commentRepository.Get(filter, orderBy);
            return query;
        }

        public IList<Comment> GetByAssociatedId(string id, string Username = null)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(Username))
                {
                    return _commentRepository.session.Query<Comment, Comments_ByAssociatedId>().Where(v => v.AssociatedId == id).ToList();
                }
                else
                {
                    return _commentRepository.session.Query<Comment, Comments_ByAssociatedId>().Where(v => v.AssociatedId == id && v.Username == Username).ToList();
                }
            }
            catch
            {
                return null;
            }
        }

        public Comment Save(Comment model)
        {
            _commentRepository.Add(model);
            return model;
        }

        public void Update(Comment model)
        {
            _commentRepository.Update(model);
        }

        public void Delete(string id)
        {
            _commentRepository.Delete(id);
        }
    }
}
