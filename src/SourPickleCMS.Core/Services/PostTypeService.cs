﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Raven.Client;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Services.DataAccess;
using SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes;

namespace SourPickleCMS.Core.Services
{
    public class ContentTypeService : IContentTypeService
    {
        private readonly IGenericRepository<ContentType> _ContentTypeRepository;

        public ContentTypeService(IDocumentSession _session)
        {
            _ContentTypeRepository = new GenericRepository<ContentType>(_session);
        }

        public IList<ContentType> Get(Expression<Func<ContentType, bool>> filter = null, Func<IQueryable<ContentType>, IOrderedQueryable<ContentType>> orderBy = null)
        {
            IList<ContentType> query = _ContentTypeRepository.Get(filter, orderBy);
            return query;
        }

        public ContentType GetBySlug(string slug)
        {
            try
            {
                return _ContentTypeRepository.session.Query<ContentType, ContentTypes_BySlug>().FirstOrDefault(pt => pt.Slug == slug);
            }
            catch
            {
                return null;
            }
        }

        public IList<ContentType> GetCustomContentTypes()
        {
            return _ContentTypeRepository.session.Query<ContentType, ContentTypes_ByCustom>().Where(pt => pt.IsCustom == true).ToList();
        }

        public ContentType Save(ContentType model)
        {
            _ContentTypeRepository.Add(model);
            return model;
        }

        public void Update(ContentType model)
        {
            _ContentTypeRepository.Update(model);
        }

        public void Delete(ContentType model)
        {
            _ContentTypeRepository.Delete(model);
        }
    }
}