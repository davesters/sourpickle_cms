﻿using SourPickleCMS.Core.Models;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
using System.Linq;

namespace SourPickleCMS.Core.Services
{
    public interface IVoteService
    {
        IList<Vote> Get(Expression<Func<Vote, bool>> filter = null, Func<IQueryable<Vote>, IOrderedQueryable<Vote>> orderBy = null);
        IList<Vote> GetByAssociatedId(string id, string Username = null);
        Vote Save(Vote model);
        void Update(Vote model);
        void Delete(string id);
    }
}