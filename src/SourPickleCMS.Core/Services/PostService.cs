﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Raven.Client;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Models.ViewModels;
using SourPickleCMS.Core.Services.DataAccess;
using SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes;
using Raven.Client.Linq;

namespace SourPickleCMS.Core.Services
{
    public class PostService : IPostService
    {
        private readonly IGenericRepository<Post> _postRepository;

        public PostService(IDocumentSession _session)
        {
            _postRepository = new GenericRepository<Post>(_session);
        }

        public IList<Post> Get(Expression<Func<Post, bool>> filter = null, Func<IQueryable<Post>, IOrderedQueryable<Post>> orderBy = null)
        {
            IList<Post> query = _postRepository.Get(filter, orderBy);
            return query;
        }

        public IList<Post> GetPostsByCategorySlug(string category)
        {
            var results = _postRepository.session.Query<Post, Posts_ByCategorySlug>().Where(p => p.Category.Slug == category && p.Status == "published").ToList();
            return results;
        }

        public IList<Post> GetPostsByTypeAndCategory(string category, string type)
        {
            var results = _postRepository.session.Query<Post, Posts_ByCategorySlug>().Where(p => p.Category.Slug == category && p.ContentType.Slug == type && p.Status == "published").ToList();
            return results;
        }

        public Post GetPostBySlugByTypeAndCategory(string slug, string category, string type)
        {
            try
            {
                return _postRepository.session.Query<Post, Posts_BySlug>().FirstOrDefault(p => p.Slug == slug && p.Category.Slug == category && p.ContentType.Slug == type && p.Status == "published");
            }
            catch
            {
                return null;
            }
        }

        public IList<Post> GetByContentTypeSlug(string type)
        {
            var results = _postRepository.session.Query<Post, Posts_ByContentTypeSlug>().Where(p => p.ContentType.Slug == type && p.Status == "published").ToList();
            return results;
        }

        public IList<Post> GetAllByContentTypeSlug(string type)
        {
            var results = _postRepository.session.Query<Post, Posts_ByContentTypeSlug>().Where(p => p.ContentType.Slug == type && p.Status != "revision").ToList();
            return results;
        }

        public Post GetBySlugAndCategory(string slug, string category)
        {
            try
            {
                return _postRepository.session.Query<Post, Posts_BySlug>().FirstOrDefault(p => p.Slug == slug && p.Category.Slug == category && p.Status == "published");
            }
            catch
            {
                return null;
            }
        }

        public Post GetBySlugAndType(string slug, string type)
        {
            try
            {
                return _postRepository.session.Query<Post, Posts_BySlug>().FirstOrDefault(p => p.Slug == slug && p.ContentType.Slug == type && p.Status == "published");
            }
            catch
            {
                return null;
            }
        }

        public Post GetBySlug(string slug)
        {
            try
            {
                return _postRepository.session.Query<Post, Posts_BySlug>().FirstOrDefault(p => p.Slug == slug && p.Status == "published" && p.ContentType.Slug != "menu-item" && p.ContentType.Slug != "categories");
            }
            catch
            {
                return null;
            }
        }

        public Post GetByID(string id)
        {
            try
            {
                return _postRepository.FirstOrDefault(p => p.Id == id);
            }
            catch
            {
                return null;
            }
        }

        public IList<Post> GetPostRevisionsByParentID(string id)
        {
            return _postRepository.session.Query<Post, Posts_ByParentID>().Where(p => p.ParentID == id && p.Status == "revision").ToList();
        }

        public int GetPostCountByType(string type)
        {
            RavenQueryStatistics stats = new RavenQueryStatistics();
            var results = _postRepository.session.Query<Post, Posts_ByContentTypeSlug>().Statistics(out stats).Where(p => p.ContentType.Slug == type && p.Status != "revision").ToArray();

            return stats.TotalResults;
        }

        public int GetPostCountByCategory(string cat, string type)
        {
            RavenQueryStatistics stats = new RavenQueryStatistics();
            var results = _postRepository.session.Query<Post, Posts_ByCategorySlug>().Statistics(out stats).Where(p => p.Category.Slug == cat && p.ContentType.Slug == type && p.Status != "revision").ToArray();

            return stats.TotalResults;
        }

        public int TotalCommentsCount()
        {
            var results = _postRepository.session.Query<Posts_CommentsCount.ReduceResult, Posts_CommentsCount>().First();

            return results.Count;
        }

        public void SaveComment(Comment model, string postid)
        {
            Post post = _postRepository.GetByID(postid);

            if (post.Comments == null) post.Comments = new List<Comment>();
            post.Comments.Add(model);
            Update(post);
        }

        public bool AddCustomField(Field model, string postid)
        {
            try
            {
                Post post = _postRepository.GetByID(postid);
                if (post != null && post.Fields != null)
                {
                    if (post.Fields.Any(f => f.Name == model.Name)) return false;
                }
                else
                {
                    post.Fields = new List<Field>();
                }
                post.Fields.Add(model);
                Update(post);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateCustomField(Field model, string postid)
        {
            try
            {
                Post post = _postRepository.GetByID(postid);
                if (post != null && post.Fields != null)
                {
                    int counter = 0;
                    int index = 0;
                    foreach (Field f in post.Fields)
                    {
                        if (f.Name == model.Name)
                        {
                            index = counter;
                            break;
                        }
                        counter++;
                    }
                    post.Fields.ElementAt(index).Value = model.Value;
                    Update(post);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteCustomField(Field model, string postid)
        {
            try
            {
                Post post = _postRepository.GetByID(postid);
                if (post != null && post.Fields != null)
                {
                    int counter = 0;
                    int index = 0;
                    foreach (Field f in post.Fields)
                    {
                        if (f.Name == model.Name)
                        {
                            index = counter;
                            break;
                        }
                        counter++;
                    }
                    post.Fields.RemoveAt(index);
                    Update(post);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public Post Save(Post model)
        {
            _postRepository.Add(model);
            return model;
        }

        public void Update(Post model)
        {
            _postRepository.Update(model);
        }

        public void Delete(Post model)
        {
            _postRepository.Delete(model);
        }
    }
}