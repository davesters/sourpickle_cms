﻿using SourPickleCMS.Core.Models;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
using System.Linq;

namespace SourPickleCMS.Core.Services
{
    public interface ILogService
    {
        IList<Log> Get(Expression<Func<Log, bool>> filter = null, Func<IQueryable<Log>, IOrderedQueryable<Log>> orderBy = null);
    }
}