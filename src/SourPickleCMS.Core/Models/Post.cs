﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SourPickleCMS.Core.Models
{
    public class Post
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Excerpt { get; set; }
        public string Body { get; set; }
        public string Slug { get; set; }
        public string TemplateName { get; set; }
        public int Views { get; set; }
        public string ParentID { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        public DateTime PublishedDate { get; set; }
        public string Status { get; set; }
        public bool CommentsClosed { get; set; }
        public string Username { get; set; }
        public int Role { get; set; }
        public bool IsApproved { get; set; }
        public string Order { get; set; }
        public int Rating { get; set; }
        public string AttachedImageId { get; set; }

        //Media Fields (Not all posts will contain these fields
        public string AltText { get; set; }
        public string Caption { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }

        public string[] Tags { get; set; }
        public Category Category { get; set; }
        public ContentType ContentType { get; set; }
        public IList<Comment> Comments { get; set; }
        public IList<Vote> Votes { get; set; }
        public IList<Field> Fields { get; set; }

        [JsonIgnore]
        private IDictionary<string, string> _values;

        [JsonIgnore]
        public IDictionary<string, string> Values
        {
            get
            {
                if (_values == null)
                {
                    if (Fields != null && Fields.Count > 0)
                    {
                        _values = new Dictionary<string, string>();
                        foreach (Field field in Fields)
                        {
                            _values.Add(field.Name, field.Value);
                        }
                    }
                }
                return _values;
            }
        }

        public Post GetBackupCopy()
        {
            Post clone = new Post
            {
                Id = "",
                Title = Title,
                Excerpt = Excerpt,
                Body = Body,
                Slug = Slug,
                TemplateName = TemplateName,
                Views = Views,
                ParentID = ParentID,
                CreateDate = CreateDate,
                LastUpdatedDate = LastUpdatedDate,
                Status = Status,
                CommentsClosed = CommentsClosed,
                Username = Username,
                IsApproved = IsApproved,
                Order = Order,
                Role = Role,
                Tags = Tags,
                Category = Category,
                ContentType = ContentType,
                Fields = Fields,
                Rating = Rating
            };

            return clone;
        }

        public string GetPermalink()
        {
            string link = "";

            if (ContentType.Slug == "media")
            {
                link = "/Uploads/";
                link += CreateDate.Year.ToString() + "/";
                link += CreateDate.Month.ToString() + "/";
                link += Slug;
            }
            else
            {
                if (ContentType != null) link += "/" + ContentType.Slug;
                if (Category != null && Category.Slug != "uncategorized") link += "/" + Category.Slug;
                link += "/" + Slug;
            }

            return link;
        }
    }
}