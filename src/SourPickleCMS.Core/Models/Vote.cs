﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SourPickleCMS.Core.Models
{
    public class Vote
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Type { get; set; }
        public DateTime VoteDate { get; set; }

        public string AssociatedId { get; set; }
    }
}