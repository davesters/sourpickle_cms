﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SourPickleCMS.Core.Models
{
    public class Comment
    {
        public string Id { get; set; }
        public string Order { get; set; }
        public string AuthorUrl { get; set; }
        public DateTime CreateDate { get; set; }
        public int Rating { get; set; }
        public string AuthorIPAddress { get; set; }
        public bool IsApproved { get; set; }
        public bool IsDeleted { get; set; }
        public IList<Vote> Votes { get; set; }

        [Required]
        public string Message { get; set; }

        [Required]
        public string Username { get; set; }

        public string AuthorEmail { get; set; }

        public string AssociatedId { get; set; }

        [JsonIgnore]
        public string PostID { get; set; }
    }
}