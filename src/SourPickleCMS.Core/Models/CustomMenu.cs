﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SourPickleCMS.Core.Models
{
    public class CustomMenu
    {
        public string Title { get; set; }
        public string Slug { get; set; }
        public string IconUrl { get; set; }
        public int Role { get; set; }
        public IList<CustomMenuItem> MenuItems { get; set; }
    }

    public class CustomMenuItem
    {
        public string Name { get; set; }
        public string Slug { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public string QueryString { get; set; }
        public int Role { get; set; }
    }
}
