﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SourPickleCMS.Core.Models
{
    public class Block
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Region { get; set; }
        public string Theme { get; set; }
        public string Author { get; set; }
        public string Website { get; set; }
        public string Description { get; set; }
        public string Directory { get; set; }
        public string Order { get; set; }
        public IDictionary<string, string> Fields { get; set; }
    }
}