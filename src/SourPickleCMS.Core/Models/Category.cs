﻿namespace SourPickleCMS.Core.Models
{
    public class Category
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public string DefaultTemplate { get; set; }
        public string ContentType { get; set; }

        public virtual int PostCount { get; set; }
    }
}