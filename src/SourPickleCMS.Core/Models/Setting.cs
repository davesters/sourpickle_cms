﻿
namespace SourPickleCMS.Core.Models
{
    public class Setting
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public bool IsCustomSetting { get; set; }
        public string Theme { get; set; }
        public string Default { get; set; }
        public string Label { get; set; }
    }
}