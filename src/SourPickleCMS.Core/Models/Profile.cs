﻿using System.Collections.Generic;
using SourPickleCMS.Core.Helpers;

namespace SourPickleCMS.Core.Models
{
    public class Profile
    {
        public string About { get; set; }
        public string Location { get; set; }
        public string Birthdate { get; set; }

        public AvatarType AvatarType { get; set; }
        public GravatarDefaultImage GravatarDefault { get; set; }
        public string AvatarFileName { get; set; }
        public string AvatarMimeType { get; set; }

        public IDictionary<string, string> CustomFields { get; set; }
    }

    public enum AvatarType
    {
        Custom,
        Gravatar
    }
}