﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace SourPickleCMS.Core.Models
{
    public class Field
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public bool IsCustomField { get; set; }
        public string GroupName { get; set; }
        public IDictionary<string, string> Options { get; set; }
        public string Order { get; set; }
    }
}