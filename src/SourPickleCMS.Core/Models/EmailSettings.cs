﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SourPickleCMS.Core.Models
{
    public class EmailSettings
    {
        public string To { get; set; }
        public string FromName { get; set; }
        public string FromAddress { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool UseAuthentication { get; set; }
        public bool UseSSL { get; set; }
    }

    public class EmailViewModel
    {
        public string Body { get; set; }
    }
}
