﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SourPickleCMS.Core.Models.ViewModels
{
    public class ValidateEmailViewModel
    {
        [Required]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Validation Code")]
        public string Code { get; set; }
    }
}