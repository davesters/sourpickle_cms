﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SourPickleCMS.Core.Models.ViewModels
{
    public class RegisterViewModel
    {
        [StringLength(254, ErrorMessage = "Name is too long.  Please correct and try again")]
        [DataType(DataType.Text)]
        [Display(Name = "User name")]
        public string Username { get; set; }

        [StringLength(254, ErrorMessage = "Name is too long.  Please correct and try again")]
        [DataType(DataType.Text)]
        [Display(Name = "Real Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(254, ErrorMessage = "Email address is too long.  Please correct and try again")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Display(Name = "Confirm Email address")]
        public string ConfirmEmail { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Passwords must be between 8 and 20 characters long, and contain both numbers and letters.")]
        [DataType(DataType.Password)]
        [RegularExpression("(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,20})$")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "I agree to the Terms of Use")]
        public bool AgreeToTerms { get; set; }
    }
}