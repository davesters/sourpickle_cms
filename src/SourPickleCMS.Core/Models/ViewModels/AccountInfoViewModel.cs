﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SourPickleCMS.Core.Models.ViewModels
{
    public class AccountInfoViewModel
    {
        public Account Account { get; set; }
    }
}