﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SourPickleCMS.Core.Models.ViewModels
{
    public class ThemeViewModel
    {
        public string Folder { get; set; }
        public IDictionary<string, string> Info { get; set; }
        public bool IsCurrent { get; set; }
    }
}