﻿using System.Collections.Generic;

namespace SourPickleCMS.Core.Models.ViewModels
{
    public class BlocksViewModel
    {
        public IList<Block> ThemeBlocks { get; set; }
        public IList<Block> AdminBlocks { get; set; }
        public IList<Block> UserBlocks { get; set; }
        public IList<string> Regions { get; set; }
    }
}
