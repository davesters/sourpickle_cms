﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SourPickleCMS.Core.Models.ViewModels
{
    public class AdminDashboardViewModel
    {
        public int PostCount { get; set; }
        public int PageCount { get; set; }
        public int UserCount { get; set; }
        public int CommentCount { get; set; }
        public int PendingCommentCount { get; set; }
        public string SiteVersion { get; set; }
        public string ThemeName { get; set; }

        public IEnumerable<Comment> RecentComments { get; set; }
    }
}