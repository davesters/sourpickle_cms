﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SourPickleCMS.Core.Models.ViewModels
{
    public class MenuListViewModel
    {
        public IList<Post> MenuItems { get; set; }
        public IList<Category> Menus { get; set; }
        public Category CurrentMenu { get; set; }
    }
}