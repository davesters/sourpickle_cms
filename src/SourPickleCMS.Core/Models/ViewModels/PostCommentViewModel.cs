﻿using System.ComponentModel.DataAnnotations;

namespace SourPickleCMS.Core.Models.ViewModels
{
    public class PostCommentViewModel
    {
        public Comment Comment { get; set; }
        public string PostID { get; set; }
        public string ReturnURL { get; set; }
    }
}