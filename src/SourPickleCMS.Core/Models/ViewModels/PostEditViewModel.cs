﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SourPickleCMS.Core.Models.ViewModels
{
    public class PostEditViewModel
    {
        public Post Post { get; set; }
        public IList<Post> Revisions { get; set; }
        public bool SaveBackup { get; set; }
    }
}