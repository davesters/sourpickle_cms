﻿using System;
using System.Collections.Generic;
using System.Web;

namespace SourPickleCMS.Core.Models
{
    public class Session
    {
        public IList<CustomMenu> CustomMenus
        {
            get
            {
                if (ContainsInSession("CustomMenus"))
                {
                    return GetFromSession("CustomMenus") as List<CustomMenu>;
                }

                return null;
            }
            set
            {
                SetInSession("CustomMenus", value);
            }
        }

        public IList<Post> Post
        {
            get
            {
                if (ContainsInSession("Post"))
                {
                    return GetFromSession("Post") as List<Post>;
                }

                return null;
            }
            set
            {
                SetInSession("Post", value);
            }
        }

        public int? CurrentPostPos
        {
            get
            {
                if (ContainsInSession("CurrentPostPos"))
                {
                    return GetFromSession("CurrentPostPos") as int?;
                }

                return 0;
            }
            set
            {
                SetInSession("CurrentPostPos", value);
            }
        }

        public IDictionary<string, string> Settings
        {
            get
            {
                if (ContainsInSession("Settings"))
                {
                    return GetFromSession("Settings") as Dictionary<string, string>;
                }

                return null;
            }
            set
            {
                SetInSession("Settings", value);
            }
        }

        public Account CurrentUser
        {
            get
            {
                if (ContainsInSession("CurrentUser"))
                {
                    return GetFromSession("CurrentUser") as Account;
                }

                return null;
            }
            set
            {
                SetInSession("CurrentUser", value);
            }
        }

        public bool LoggedIn
        {
            get
            {
                if (ContainsInSession("LoggedIn"))
                {
                    if ((bool)GetFromSession("LoggedIn"))
                        return true;
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                SetInSession("LoggedIn", value);
            }
        }


        // Main Session CRUD Functions
        public void ClearSession()
        {
            HttpContext.Current.Session.Clear();
        }

        public bool ContainsInSession(string key)
        {
            if (HttpContext.Current.Session != null && HttpContext.Current.Session[key] != null)
                return true;
            return false;
        }

        public void RemoveFromSession(string key)
        {
            HttpContext.Current.Session.Remove(key);
        }

        private string GetQueryStringValue(string key)
        {
            return HttpContext.Current.Request.QueryString.Get(key);
        }

        private void SetInSession(string key, object value)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null)
            {
                return;
            }
            HttpContext.Current.Session[key] = value;
        }

        private object GetFromSession(string key)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null)
            {
                return null;
            }
            return HttpContext.Current.Session[key];
        }

        private void UpdateInSession(string key, object value)
        {
            HttpContext.Current.Session[key] = value;
        }
    }
}