﻿using System;

namespace SourPickleCMS.Core.Models
{
    public class Log
    {
        public string Id { get; set; }
        public string Message { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
