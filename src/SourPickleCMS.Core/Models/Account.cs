﻿using System;
using Newtonsoft.Json;

namespace SourPickleCMS.Core.Models
{
    public class Account
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public int Karma { get; set; }
        public string Name { get; set; }
        public int Role { get; set; }
        public DateTime JoinDate { get; set; }
        public DateTime LastLoginDate { get; set; }
        public string EmailVerification { get; set; }
        public bool EmailVerified { get; set; }
        public bool AgreeToTerms { get; set; }
        public bool IsLocked { get; set; }
        public bool IsSuspended { get; set; }
        public bool RegistrationComplete { get; set; }
        public string LockedReason { get; set; }
        public string IPAddress { get; set; }
        public string Url { get; set; }

        public Profile Profile { get; set; }

        public bool Login(string pass)
        {
            string hashedPassword = SecurityHelper.GetHashedString(pass + "|" + PasswordSalt);

            if (hashedPassword == Password)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void CreateNewPassword()
        {
            PasswordSalt = SecurityHelper.GetRandomString(20);
            Password = SecurityHelper.GetHashedString(Password + "|" + PasswordSalt);
        }
    }
}