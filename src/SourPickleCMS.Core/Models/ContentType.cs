﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace SourPickleCMS.Core.Models
{
    public class ContentType
    {
        public string Id { get; set; }
        public string Slug { get; set; }
        public string Name { get; set; }
        public string PluralName { get; set; }
        public bool Commentable { get; set; }
        public string DefaultTemplate { get; set; }
        public bool IsCustom { get; set; }
        public bool UsesEditor { get; set; }
        public IList<Field> Fields { get; set; }
    }
}