﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using System.ComponentModel;
using SourPickleCMS.Core.Services.DataAccess;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Attributes;
using SourPickleCMS.Core.Models.ViewModels;
using SourPickleCMS.Core.Extensions;

namespace SourPickleCMS.Core.Controllers
{
    public class PostController : SourPickleController
    {
        public ActionResult Index([DefaultValue("")]string id)
        {
            IList<Post> results = new List<Post>();
            Category category = null;
            ContentType ContentType = null;
            bool resultsFound = false;

            // get slug from url string
            string slug = Request.RawUrl.Substring(1);

            if (slug.Trim() != "")
            {
                // Split slug by '/'s into string array
                string[] tempslugs = slug.Split('/');
                IList<string> slugs = new List<string>();
                foreach (string s in tempslugs)
                {
                    if (!String.IsNullOrEmpty(s)) slugs.Add(s);
                }

                if (slugs.Count() > 1)
                {
                    // Check if it's a Content Type
                    string typeslug = slugs[0];
                    ContentType = unitOfWork.ContentTypeRepository.GetBySlug(typeslug);

                    if (ContentType != null)
                    {
                        // Check the second value to see if it's a category
                        string catslug = slugs[1];
                        category = unitOfWork.CategoryRepository.GetBySlug(catslug, ContentType.Slug);
                    }
                }

                // If there is more than one slug then the first one must be a Content Type or a category
                if (slugs.Count() > 1 && (ContentType != null || category != null))
                {
                    // Get post results
                    if (slugs.Count() > 2 && ContentType != null && category != null) // If the slug count has a third value and the type and category exist, check for the matching post
                    {
                        string postSlug = slugs[2];
                        Post result = unitOfWork.PostRepository.GetPostBySlugByTypeAndCategory(postSlug, category.Slug, ContentType.Slug);
                        if (result != null) results.Add(result);
                    }
                    else if (ContentType != null && category != null) // If Content Type and category exists, then get the posts in the category
                    {
                        results = unitOfWork.PostRepository.GetPostsByTypeAndCategory(category.Slug, ContentType.Slug).OrderBy(o => o.CreateDate).ToList();
                        resultsFound = true;
                    }
                    else if (ContentType != null && category == null) // If the Content Type exists, but the category does not, then check for a matching uncategorized post
                    {
                        string postSlug = slugs[1];
                        Post result = unitOfWork.PostRepository.GetPostBySlugByTypeAndCategory(postSlug, "uncategorized", ContentType.Slug);

                        // If no results were not found in uncategorized, then get the first matching slug in all of the matching Content Type posts
                        if (result == null)
                        {
                            result = unitOfWork.PostRepository.GetBySlugAndType(postSlug, ContentType.Slug);
                            if (result != null) results.Add(result);
                        }
                        else
                        {
                            results.Add(result);
                        }
                    }
                }
                else
                {
                    Post result;

                    if (!String.IsNullOrWhiteSpace(id))
                    {
                        result = unitOfWork.PostRepository.GetByID(id);
                    }
                    else
                    {
                        // Check if a post matches the slug provided
                        result = unitOfWork.PostRepository.GetBySlug(slugs[0]);
                    }

                    if (result == null) // If no post matches then check the Content Type
                    {
                        ContentType = unitOfWork.ContentTypeRepository.GetBySlug(slugs[0]);
                        if (ContentType != null)
                        {
                            results = unitOfWork.PostRepository.GetByContentTypeSlug(ContentType.Slug);
                            resultsFound = true;
                        }
                    }
                    else
                    {
                        results.Add(result);
                    }
                }
            }
            else
            {
                Post homepage = unitOfWork.PostRepository.GetBySlugAndType("home", "page");
                if (homepage != null) results.Add(homepage);
            }

            if ((results == null || results.Count() == 0) && resultsFound == false) return View("PageNotFound");

            // Process results
            unitOfWork.Session.Post = results;
            unitOfWork.Session.CurrentPostPos = 0;
            if (results.Count() != 1)
            {
                // More than one or no results were found.  Display the default or specified template.
                foreach (Post p in results)
                {
                    if (p.Role > unitOfWork.Session.CurrentUser.Role) return View("AccessDenied");
                }

                ViewData.Model = results;
                string template = "Default";

                if (category != null)
                {
                    template = category.DefaultTemplate;
                    ViewBag.Title = category.Name;
                }
                if (ContentType != null)
                {
                    if (results.Count() > 0)
                    {
                        if (!String.IsNullOrWhiteSpace(results.First().TemplateName)) template = results.First().TemplateName;
                        ViewBag.Title += " " + results.First().ContentType.PluralName;
                    }
                    else
                    {
                        if (!String.IsNullOrWhiteSpace(ContentType.DefaultTemplate)) template = ContentType.DefaultTemplate;
                        ViewBag.Title += " " + ContentType.PluralName;
                    }
                }

                return new PageTemplateActionResult(pageTemplate: template);
            }
            else
            {
                // Only one result was found.  Display it using the specified template.
                if (results.First().Role > unitOfWork.Session.CurrentUser.Role) return View("AccessDenied");

                string template = "Default";
                if (!String.IsNullOrWhiteSpace(results.First().TemplateName))
                {
                    template = results.First().TemplateName;
                }
                else
                {
                    if (category != null)
                    {
                        template = category.DefaultTemplate;
                    }
                    else if (ContentType != null)
                    {
                        template = ContentType.DefaultTemplate;
                    }
                }

                ViewData.Model = results;
                ViewBag.Title = results.First().Title;
                return new PageTemplateActionResult(pageTemplate: template);
            }
        }

        [HttpPost]
        public ActionResult PostComment(Comment model)
        {
            if (ModelState.IsValid)
            {
                model.AuthorIPAddress = Request.UserHostAddress;

                unitOfWork.PostRepository.SaveComment(model, model.PostID);
            }
            else
            {
                TempData["error"] = "Please complete all required fields and try again.";
            }

            return Redirect(Request.UrlReferrer.AbsoluteUri);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            this.ActionInvoker.InvokeAction(this.ControllerContext, "Index");
        }
    }
}