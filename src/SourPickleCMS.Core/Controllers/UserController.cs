﻿using System;
using System.Linq;
using System.Web.Mvc;
using SourPickleCMS.Core.Services.DataAccess;
using SourPickleCMS.Core.Models.ViewModels;
using SourPickleCMS.Core.Models;

namespace SourPickleCMS.Core.Controllers
{
    public class UserController : SourPickleController
    {
        public ActionResult Index()
        {
            if (unitOfWork.Session.LoggedIn)
            {
                return RedirectToAction("Profile");
            }
            else
            {
                return View("LogOn");
            }
        }

        public ActionResult LogOn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogOn(LogOnViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                Account account = unitOfWork.AccountRepository.GetByUsername(model.Username);

                if (account != null)
                {
                    if (account.IsLocked) return View("Locked");

                    if (account.Login(model.Password))
                    {
                        if (model.RememberMe == true && Request.Browser.Cookies)
                        {
                            Response.Cookies["SOURPICKLELOGIN"].Expires = DateTime.Now.AddDays(30);
                            Response.Cookies["SOURPICKLELOGIN"]["AUTOLOGIN"] = SecurityHelper.GetSecureCookieString(model.Username, account.PasswordSalt, DateTime.Now.AddDays(30));
                        }
                        account.LastLoginDate = DateTime.Now;
                        unitOfWork.Session.CurrentUser = account;
                        unitOfWork.Session.LoggedIn = true;

                        if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                            && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "The email address or password provided is incorrect.");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The email address or password provided is incorrect.");
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult LogOff()
        {
            try
            {
                Response.Cookies.Clear();
                Request.Cookies.Clear();
                Response.Cookies["SOURPICKLELOGIN"].Expires = DateTime.Now.AddDays(-1);
                Response.Cookies["SOURPICKLELOGIN"]["AUTOLOGIN"] = null;
                unitOfWork.Session.ClearSession();
            }
            catch { }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            if (unitOfWork.Session.LoggedIn == true)
            {
                return RedirectToAction("Home");
            }

            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (unitOfWork.Session.Settings["RequireAgreeToTerms"] == "true" && model.AgreeToTerms == false)
                {
                    ModelState.AddModelError("", "You must agree to the Terms and Conditions.");
                    return View(model);
                }
                // Attempt to register the user
                Account account = new Account { Email = model.Email, Password = model.Password, IPAddress = Request.UserHostAddress, Role = 2 };
                account = unitOfWork.AccountRepository.CreateNewUser(account);

                if (account != null)
                {
                    unitOfWork.Session.CurrentUser = account;
                    unitOfWork.Session.LoggedIn = true;
                    return RedirectToAction("Home");
                }
                else
                {
                    ModelState.AddModelError("", "An account for that e-mail address already exists. Please enter a different e-mail address.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool changePasswordSucceeded = true;

                try
                {
                    Account currentUser = unitOfWork.Session.CurrentUser;
                    if (currentUser.Login(model.OldPassword) == false)
                    {
                        ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                        changePasswordSucceeded = false;
                    }
                    else
                    {
                        currentUser.Password = SecurityHelper.GetHashedString(model.NewPassword + "$" + currentUser.PasswordSalt);
                        unitOfWork.AccountRepository.Update(currentUser);
                        unitOfWork.Session.CurrentUser.Password = currentUser.Password;
                    }
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return View("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }
            return View(model);
        }

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        public ActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                Account account = unitOfWork.AccountRepository.GetByEmail(model.Email);

                if (account != null)
                {
                    if (account.LockedReason != "failed logins") return View("Locked");

                    account.LockedReason = null;
                    account.IsLocked = false;
                    string tempPass = SecurityHelper.GetRandomString(12);
                    account.Password = SecurityHelper.GetHashedString(tempPass + "$" + account.PasswordSalt);

                    unitOfWork.AccountRepository.Update(account);

                    TempData["Password"] = tempPass;
                    return View("ResetPasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "Could not find an account associated with that e-mail address.");
                }
            }

            return View(model);
        }

        public ActionResult ResetPasswordSuccess()
        {
            return View();
        }

        public ActionResult Locked()
        {
            return View();
        }

        public ActionResult Suspended()
        {
            return View();
        }
    }
}
