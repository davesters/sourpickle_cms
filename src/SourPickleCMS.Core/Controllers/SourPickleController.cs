﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Raven.Client;
using Raven.Client.Document;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Services.DataAccess;
using System.IO;
using SourPickleCMS.Core.Helpers;

namespace SourPickleCMS.Core.Controllers
{
    public abstract class SourPickleController : Controller
    {
        public IUnitOfWork unitOfWork;

        public ActionResult PageNotFound()
        {
            return View("PageNotFound");
        }

        protected string RenderBlockToString(string view, object model)
        {
            if (string.IsNullOrEmpty(view)) return "";

            ViewData.Model = model;

            try
            {
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, view);
                    ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.GetStringBuilder().ToString();
                }
            }
            catch
            {
                return "";
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            unitOfWork = new UnitOfWork((IDocumentSession)HttpContext.Items["CurrentDocumentSession"]);
            SecurityHelper.ProcessAutoLogin(Request);

            if (unitOfWork.Session.Settings == null)
            {
                unitOfWork.Session.Settings = unitOfWork.SettingRepository.GetSettingsDictionary();
            }
            if (!unitOfWork.Session.Settings.ContainsKey("Scripts"))
            {
                unitOfWork.Session.Settings.Add("Scripts", "");
            }
            else
            {
                unitOfWork.Session.Settings["Scripts"] = "";
            }

            if (unitOfWork.Session.LoggedIn && unitOfWork.Session.CurrentUser.IsSuspended && filterContext.ActionDescriptor.ActionName != "Suspended")
            {
                unitOfWork.Session.LoggedIn = false;
                unitOfWork.Session.CurrentUser = new Account { Role = 0 };
                filterContext.Result = new RedirectResult("/User/Suspended");
                base.OnActionExecuting(filterContext);
                return;
            }
            if (unitOfWork.Session.LoggedIn && unitOfWork.Session.CurrentUser.IsLocked && unitOfWork.Session.CurrentUser.IsSuspended == false && filterContext.ActionDescriptor.ActionName != "Locked")
            {
                if (filterContext.ActionDescriptor.ActionName == "ResetPassword" && unitOfWork.Session.CurrentUser.LockedReason == "failed logins")
                {
                    base.OnActionExecuting(filterContext);
                    return;
                }

                unitOfWork.Session.LoggedIn = false;
                unitOfWork.Session.CurrentUser = new Account { Role = 0 };
                filterContext.Result = new RedirectResult("/User/Locked");
                base.OnActionExecuting(filterContext);
            }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.IsChildAction) return;
            if (filterContext.Exception != null) return;
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            if (unitOfWork.Session.LoggedIn)
            {
                filterContext.Controller.ViewBag.Loggedin = true;
                filterContext.Controller.ViewBag.Account = unitOfWork.Session.CurrentUser;
            }
            else
            {
                filterContext.Controller.ViewBag.Loggedin = false;
                filterContext.Controller.ViewBag.Account = new Account { Role = 0 };
            }
            filterContext.Controller.ViewBag.AppSettings = unitOfWork.Session.Settings;
        }

        protected void SendEmail(string to, string fromname, string fromaddress, string subject, string message, bool async = true)
        {
            EmailSettings settings = new EmailSettings
            {
                FromName = (!String.IsNullOrWhiteSpace(fromname)) ? fromname : unitOfWork.Session.Settings["EmailFrom"],
                FromAddress = (!String.IsNullOrWhiteSpace(fromaddress)) ? fromaddress : unitOfWork.Session.Settings["EmailFromAddress"],
                Subject = subject,
                Port = Convert.ToInt32(unitOfWork.Session.Settings["EmailPort"]),
                Host = unitOfWork.Session.Settings["EmailHost"],
                UseSSL = Convert.ToBoolean(unitOfWork.Session.Settings["EmailUseSSL"]),
                UseAuthentication = Convert.ToBoolean(unitOfWork.Session.Settings["EmailAuthentication"]),
                Username = unitOfWork.Session.Settings["EmailUsername"],
                Password = unitOfWork.Session.Settings["EmailPassword"],
                To = to
            };
            message = message.Replace("\r\n", "<br />");
            message = message.Replace("\n", "<br />");
            settings.Body = RenderBlockToString("Email/_GeneralEmail.html", new EmailViewModel { Body = message });

            using (EmailSender sender = new EmailSender(settings))
            {
                if (async)
                {
                    sender.SendAsync();
                }
                else
                {
                    sender.Send();
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (unitOfWork != null) unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}