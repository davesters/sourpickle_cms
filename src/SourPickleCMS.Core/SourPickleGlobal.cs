﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Indexes;
using SourPickleCMS.Core.Controllers;
using SourPickleCMS.Core.Extensions;
using SourPickleCMS.Core.Models;
using SourPickleCMS.Core.Services.DataAccess;
using SourPickleCMS.Core.Services.DataAccess.RavenDB.Indexes;
using SourPickleCMS.Core.Utils;

namespace SourPickleCMS.Core
{
    public class SourPickleApplication : HttpApplication
    {
        public static IDocumentStore Store { get; private set; }

        protected SourPickleApplication()
        {
            BeginRequest += (sender, args) =>
            {
                HttpContext.Current.Items["CurrentDocumentSession"] = Store.OpenSession();
            };
            EndRequest += (sender, args) =>
            {
                using (var session = (IDocumentSession)HttpContext.Current.Items["CurrentDocumentSession"])
                {
                    if (session == null)
                        return;

                    if (Server.GetLastError() != null)
                        return;
                }
            };
        }

        public static void RegisterSourPickleGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void SourPickleRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*allaxd}", new { allaxd = @".*\.axd(/.*)?" });
            routes.IgnoreRoute("pingback");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });

            // General Routes
            routes.MapRoute("Search", "Search", new { controller = "Search", action = "Post" });

            // Default Routes
            routes.MapRoute(
                "DefaultHome", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
            routes.MapRoute(
                "DefaultPost", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Post", action = "Index", id = UrlParameter.Optional }, // Parameter defaults
                new string[] { "SourPickleCMS.Core.Controllers" }
            );

            // Routing catchall for posts
            routes.MapRoute("General", "{*slug}", new { controller = "Post", action = "Index" });
        }

        protected virtual void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterSourPickleGlobalFilters(GlobalFilters.Filters);
            InitializeDocumentStore();
            SeedDocumentStore();

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new CustomViewEngine());

            GlobalFilters.Filters.Add(new UseAntiForgeryTokenOnPostByDefault());
            SourPickleRoutes(RouteTable.Routes);
            ControllerBuilder.Current.SetControllerFactory(new ControllerFactoryWrapper(ControllerBuilder.Current.GetControllerFactory()));
        }

        private static void InitializeDocumentStore()
        {
            if (Store != null) return; // prevent misuse
            string env = ConfigurationManager.AppSettings["Environment"];
            string user = ConfigurationManager.AppSettings["RavenUsername"];
            string pass = ConfigurationManager.AppSettings["RavenPassword"];

            if (!String.IsNullOrWhiteSpace(env) && !String.IsNullOrWhiteSpace(user))
            {
                Store = new DocumentStore { ConnectionStringName = env + "DocumentStore", Credentials = new NetworkCredential(user, pass)  }.Initialize();
            }
            else if (!String.IsNullOrWhiteSpace(env))
            {
                Store = new DocumentStore { ConnectionStringName = env + "DocumentStore" }.Initialize();
            }
            else
            {
                Store = new DocumentStore { ConnectionStringName = "LiveDocumentStore" }.Initialize();
            }
            TryCreatingIndexesOrRedirectToErrorPage();

            Store.Conventions.IdentityPartsSeparator = "-";
            Store.Conventions.SaveEnumsAsIntegers = true;
        }

        public static void RegisterIndex<T>() where T : class
        {
            IndexCreation.CreateIndexes(typeof(T).Assembly, Store);
        }

        private static void TryCreatingIndexesOrRedirectToErrorPage()
        {
            try
            {
                IndexCreation.CreateIndexes(typeof(Posts_BySlug).Assembly, Store);
            }
            catch (WebException e)
            {
                var socketException = e.InnerException as SocketException;
                if (socketException == null)
                    throw;

                switch (socketException.SocketErrorCode)
                {
                    case SocketError.AddressNotAvailable:
                    case SocketError.NetworkDown:
                    case SocketError.NetworkUnreachable:
                    case SocketError.ConnectionAborted:
                    case SocketError.ConnectionReset:
                    case SocketError.TimedOut:
                    case SocketError.ConnectionRefused:
                    case SocketError.HostDown:
                    case SocketError.HostUnreachable:
                    case SocketError.HostNotFound:
                        HttpContext.Current.Response.Redirect("/Home/DatabaseNotReachable");
                        break;
                    default:
                        throw;
                }
            }
        }

        private static void SeedDocumentStore()
        {
            using (IDocumentSession session = Store.OpenSession())
            {
                var user = session.Load<Account>("accounts-1");
                if (user != null)
                {
                    return;
                }

                DatabaseInitializer.Initialize((DocumentSession)session);

                // Wait for all stale indexes to complete.
                while (Store.DatabaseCommands.GetStatistics().StaleIndexes.Length > 0)
                {
                    Thread.Sleep(10);
                }
            }
        }
    }
}